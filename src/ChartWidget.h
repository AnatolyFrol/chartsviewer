//! @file ChartWidget.h
//! Объявление классов chart::CGroupOfChart, chart::CChartWidget,
//! chart::CBoundedTimeOperator, chart::CDisplayTimeOperator, chart::CLinesPainter 
//! также enum chart::EChannelState, struct chart::ChannelMarkProperty

#ifndef _CCHARTWIDGET_H
#define _CCHARTWIDGET_H

#include <map>

#include <QPainter>
#include <qpixmap.h>
#include <QScrollArea>
#include <QGraphicsView>
#include <QList>
#include <QLabel>
#include <QtGui/QWidget>

#include "../includes/Time.h"
#include "../includes/DataId.h"
#include "../includes/Event.h"

#include <qline.h>

namespace chart
{

//! Индекс окна для отображения импульсных событий
const size_t EVENT_MODE_INDEX = 1;
//! Индекс окна длдя отображения длительных событий
const size_t FRAME_MODE_INDEX = 0;

//! Перечисления возможных типов сигналов.
enum EChannelState
{
    CS_LOW   = 0x00,
    CS_HIGH  = 0x01,
    CS_PULSE = 0x02
};

//! Тип контейнера для хранения списка событий.
typedef std::vector<CEvent> ChannelRecords;
//! Тип контейнера для связи ассоциативного кода канала с его массивом сигналов.
//! Ключ - идентификатор канала, значение - ассоц. массив сигналов.
typedef std::map <uint32_t, ChannelRecords> ChartRecords;

//! Класс хранения и доступа к событиям каналов
class CGroupOfChart
{
private:
    ChartRecords m_eventChart;
    ChartRecords m_frameChart;

    int m_currentIndex;
    int m_count;//const
    std::vector<uint32_t> m_codeByIndex[2];
    bool m_isMoscowTime;
public:
    CGroupOfChart();
    int GetChannelCount();
    bool IsEventChart();
    const ChannelRecords& GetChannelByIndex(int idx);
    uint32_t GetCodeByIndex(int idx);
    void SelectNext();
    void InitChartEvets(std::vector<std::pair<DataId, std::string>> events);
    void InitChartFrames(std::vector<std::pair<DataId, std::string>> frames);
    void SetFrameSignal(const std::vector<CEvent>& events);
    void SetEventSignal(const std::vector<CEvent>& events);

    static EChannelState DataIdToEchannelState(DataId id)
    {
        if (id.type == DataId::DT_EVENT_LONG)
        {
            if (id.index == 0)
            {
                return CS_HIGH;
            }
            else
            {
                return CS_LOW;
            }
        }
        return CS_PULSE;
    }
};

//! Окно просмотра временных диаграмм
class CChartWidget : public QGraphicsView
{
    Q_OBJECT
public:
    CChartWidget(QWidget* parent = 0);

signals:
    void EvtTextVerticalPos(int position);
    void TimeMarkChanged(QPoint position);
    void EvtTempTimePosition(int position);

public:
    void SetHeight(int height);
    void SetPixmap(QPixmap pixmap);
    void SetTimeMark(int position);
    void HideTimeMark();
    void BindingPoints(std::vector<int> *points);

public:
    virtual void mouseMoveEvent(QMouseEvent *pEvent);
    virtual void mousePressEvent(QMouseEvent *pEvent);


private:
    QGraphicsLineItem*   m_pTimeMarkLine;
    QGraphicsLineItem*   m_pVerticalLine;
    QGraphicsScene*      m_pScene;
    QGraphicsPixmapItem* m_pPixmapItem;
    std::vector<int>*    m_pBindingPoints;

};

//! Класс хранения, доступа и расчета основных временных параметров просмотриваеммых диаграмм
class CBoundedTimeOperator
{
public:
    CBoundedTimeOperator()
    {
    }

public:
    void SetWorkSpan(TimeRange workSpan);
    void SetAllSpan(TimeCyclic start, TimeCyclic finish);
    void SetCenter(TimeCyclic time);
    TimeCyclic GetDuration();
    TimeCyclic GetAllDuration();
    TimeRange GetCurrentTimeRange();
    void VisibleScreen(TimeCyclic duration);
    void ComputeTimeRange(int* coef);
    void ShiftCenter(int64_t shift);
    void SetHorizontScroll(int position);
    bool IsVisible(TimeCyclic time);

private:
    TimeRange m_allTimeRange;
    TimeRange m_currentTimeRange;
    TimeCyclic m_duration;
    TimeCyclic m_allDuration;
    TimeCyclic m_center;
    int        m_position;

    uint64_t m_coefConversion;
};

//! Класс для передачи данных времени View-Model и для отображения времени в строке
class CDisplayTimeOperator
{
public:
    CDisplayTimeOperator() : m_isMoscowTime(false)
    {
    }

public:
    void SetWorkSpan(TimeRange workSpan);
    bool IsVisible(TimeCyclic time, EChannelState state);
    uint64_t GetXCoordinate(TimeCyclic time, EChannelState state);
    int TranslateTimeToXCoordinate(TimeCyclic time);
    TimeCyclic GetVisibleDuration();
    TimeRange  GetVisibleRange();
    TimeCyclic GetTimeFromScreen(int x);
    QString GetTimeTextFromScreen(int x);
    void SetWidth(int inWidth);
    void SetMoscowTimeVector(boost::shared_ptr<MoscowTimeVector> pMoscowTimeVector);
    void SetVisibleTimeMode(bool isMoscowTime);
    QString CreateTextFromTime(TimeCyclic time);

private:
    uint64_t m_width;

    TimeCyclic m_startTime;
    TimeCyclic m_finishTime;
    TimeCyclic m_duration;
    boost::shared_ptr<MoscowTimeVector> m_pMoscowTimeVector;
    bool m_isMoscowTime;
};

//! Класс рисующий картинку с диаграммами каналов
class CLinesPainter
{
public:
    const static int yPadding       = 5;  // отступы (всерху и снизу)
    const static int channelHeight  = 20; // высота одного графика (события)
    const static int bandHeight     = 30; // вся высота для одного графика
    const static int scaleTextWidth = 70; // ширина надписи на шкале
    const static int scaleMarkWidth = 7;  // зазор между черточками шкалы - кратна scaleTextWidth

public:
    CLinesPainter();
    ~CLinesPainter();
    void AddPoint(TimeCyclic time, uint32_t dataId, EChannelState state);
    QPixmap GetPixmap();
    void SelectChannel(int indexSelected);

    void SetTimeOperator(CDisplayTimeOperator *pTimeOperator);
    void SetWidth(QSize size);
    void SetIdList(std::vector<std::pair<DataId, std::string>> idList);
    void Clear();

private:
    //! Класс для хранения информации о рисуемых прямоугольниках (событиях)
    class PointOfLine
    {
    public:
        uint64_t X;
        int yIndex;
        int state;
        TimeCyclic time;
        QColor color;

        PointOfLine()
        {
            yIndex = -1;
        }
        PointOfLine(uint64_t x, int y, int st, TimeCyclic tm, QColor cl = QColor(Qt::red)) :
            X(x), yIndex(y), state(st), time(tm), color(cl)
        {
        }
    };

private:
    typedef std::map<uint32_t, boost::shared_ptr<PointOfLine> > ChannelPoint;

    QPixmap m_pixmap;

    QPainter* m_painter;

    ChannelPoint m_lastPoint;

    int m_yLastIndex;
    uint64_t m_width;

    CDisplayTimeOperator *m_pTimeOperator;

    TimeRange m_allRange;

    TimeCyclic DefineTimeDelta(uint64_t duration, uint32_t width);
    void PaintScale();
    void PaintRect(const PointOfLine* pnt1, const PointOfLine* pnt2);
};

//! Класс свойств метки и выделенного канала: индекс, переходы влево/вправо, время и коорд. метки
struct ChannelMarkProperty
{
    ChannelMarkProperty() : leftShift(false), rightShift(false), xPosition(0), channelIndex(-1),
                            timeMark(0)
    {
    }

    bool leftShift;
    bool rightShift;
    int xPosition;
    int channelIndex;
    uint32_t channelCode;
    TimeCyclic timeMark;
};

}

#endif // _CCHARTWIDGET_H
