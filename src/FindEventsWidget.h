//! @file FindEventsWidget.h
//! Объявление класса chart::CFindEventsWidget.

#ifndef _FIND_EVENTS_WIDGET_H
#define _FIND_EVENTS_WIDGET_H

#include <QtGui/QWidget>
#include <QVBoxLayout>
#include <QPushButton>

#include "ChartWidget.h"

namespace chart
{

//! Класс окна "Поиск событий".
class CFindEventsWidget : public QWidget
{
    Q_OBJECT
public:
    CFindEventsWidget(QWidget *parent = 0);
    ~CFindEventsWidget();
    void SetHorizontalScroll(int max, int step, int value);
    void UpdatePixmap(const QPixmap& pixmap);
    void SetHeight(int height);
    void SetTimeMark(ChannelMarkProperty markProperty);
    void UpdateScreen(ChannelMarkProperty markProperty);
    void SetTimeOperator(CDisplayTimeOperator *pTimeOperator);
    void BindingPoints(std::vector<int> *pPoints);

    void DrawText(std::vector<std::pair<DataId, std::string>> nameIdList);

    void RetranslateUi();

signals :
    void ChartHorizontalScrollBar(int position);
    void ShiftHorizontalScroll(int shift);
    void SizeChanged(QSize newSize);
    void EvtTimeMarkChanged(QPoint position);
    void EvtChannelSelection(int yPosition);
    void EvtTempTimePosition(int position);
    void EvtTimeMarkShift(int shift);
    void ZoomIn();
    void ZoomOut();
    void EvtChangeMode();

protected:
    virtual void mouseMoveEvent(QMouseEvent *pEvent);
    virtual void mousePressEvent(QMouseEvent *pEvent);
    virtual void resizeEvent(QResizeEvent* pEvent);
    virtual void keyPressEvent(QKeyEvent *pEvent);

private:
    CChartWidget*   m_pChart;
    QScrollArea*    m_pNames;
    QScrollBar*     m_pHorizScrollBar;
    QLabel*         m_pTimeScreen;
    QPushButton*    m_buttonMode;

    CDisplayTimeOperator *m_pTimeOperator;
    QString m_markTimeStr;
    QString m_cursorTimeStr;

    QVBoxLayout *m_pVirtBoxLayout;
    int m_selectedIndex;

    void UpdateTimeLabel();

private slots:
    void OnTextVerticalPos(int position);
    void OnChartHorizontalPos(int postion);
    void OnTimeMarkChanged(QPoint position);
    void OnTempTimePosition(int position);
    void OnShiftMarkRight();
    void OnShiftMarkLeft();
    void OnChangeMode();
};

} // namespace chart


#endif // _FIND_EVENTS_WIDGET_H
