//! @file Log.h
//! Объявление и реализация класса CLog.

#ifndef _INC_LOG_H
#define _INC_LOG_H

#include <stdint.h>

#include "Singleton.h"
#include "Format.h"

#ifdef _MSC_VER
extern "C" __declspec(dllimport) void __stdcall OutputDebugStringA(const char *);
#endif

#include <boost/thread.hpp>
#include <boost/date_time.hpp>
#include <boost/filesystem.hpp>

#include <stdio.h>
#include <stdarg.h>

//! Ошибка критическая для работы программы.
const uint32_t LOG_FATAL = 0; 
//! Ошибка критическая для определенной функции программы.
const uint32_t LOG_ERR   = 1; 
//! Предупреждение о возможных неполадках в работе программы или комплекса в целом.
const uint32_t LOG_WARN  = 2; 
//! Информационное сообщение. Например, информация о входящих сообщениях.
const uint32_t LOG_INFO  = 3; 
//! Отладочная информация.
const uint32_t LOG_DBG	 = 4; 

//! Уровень вербальности сообщений
const uint32_t LOG_DEFAULT_LEVEL = LOG_INFO;

//! Класс для ведения лога. Реализован как синглтон.
class CLog
{
    friend class CSingletonHolder<CLog>;

public:
    //! Задает файл лога.
    //! @param fileName - [in] имя файла;
    //! @param maxSize  - [in] наибольший размер файла лога, в байтах (0 - размер не ограничен).
    //! @note Если размер лога превышает заданный размер, файл лога закрывается,
    //!       имя файла дополняется ".1", открывается новый файл лога с исходным именем,
    //!       в него продолжается запись.
    void SetFile(const std::string& fileName, size_t maxSize = 0)
    {
        FILE* file = Open(fileName);

        if ((!file) && (!fileName.empty()))
        {
            Write(LOG_ERR, "Failed to open log file: %s", fileName.c_str());
            return;
        }

        boost::unique_lock<boost::detail::spinlock> lock(m_lock);
        Close();

        m_fileName = fileName;
#ifdef _MSC_VER
        m_file = file;
#else
        m_file = (fileName.empty()) ? stderr : file;
#endif
        m_maxSize  = maxSize;
        m_size     = 0;
    }
    
    //! Задает уровень сообщений. Только сообщения с уровнем меньшим или равным заданному 
    //! фиксируются в лог.
    //! @param level - [in] уровень сообщений
    void SetLevel(uint32_t level)
    {
        m_level = level;
    }
    
    //! Пишет строку в лог.
    //! @param level  - [in] уровень сообщения
    //! @param format - [in] формат строки
    void Write(uint32_t level, const char* format, ...)
    {
        if (level > m_level)
        {
            return;
        }

        va_list args;
        va_start(args, format);

        std::string message = FormatStringV(format, args);
        
        va_end(args);

        Write(level, message);
    }

    //! Пишет строку в лог.
    //! @param level   - [in] уровень сообщения;
    //! @param message - [in] строка сообщения.
    void Write(uint32_t level, const std::string& message)
    {
        if (level > m_level)
        {
            return;
        }

        boost::system_time time = boost::get_system_time();

        std::string str = boost::posix_time::to_simple_string(time) + " " + message;

        boost::unique_lock<boost::detail::spinlock> lock(m_lock);

        if ((m_file) && (m_maxSize != 0) && (m_size + str.size() + 1 > m_maxSize))
        {
            Close();
            Backup();
            m_file = Open(m_fileName);
            m_size = 0;
        }

        if (m_file)
        {
            fprintf(m_file, "%s\n", str.c_str());
            fflush(m_file);
        }
        else
        {
            printf("%s\n", str.c_str());
        }

        m_size += str.size() + 1;

#ifdef _MSC_VER
        OutputDebugStringA(str.c_str());
        OutputDebugStringA("\n");
#endif
    }

private:
    uint32_t                m_level;
    FILE*                   m_file;
    boost::detail::spinlock m_lock;
    std::string             m_fileName;
    size_t                  m_maxSize;
    size_t                  m_size;

private:
    //! Закрытый конструктор.
    CLog() :
        m_level(LOG_DEFAULT_LEVEL),
        m_maxSize(0),
#ifdef _MSC_VER
        m_file(NULL)
#else
        m_file(stderr)
#endif
    {
        boost::detail::spinlock sp = BOOST_DETAIL_SPINLOCK_INIT;
        m_lock = sp;
    }

    //! Закрытый конструктор копирования.
    CLog(const CLog&) {}

    //! Деструктор.
    ~CLog()
    {
        Close();
    }

    //! Открывает уканный файл.
    //! @param fileName - [in] имя файла;
    //! @return объект файла или NULL - если файл открыть не удалось.
    FILE* Open(const std::string& fileName)
    {
        if (fileName.empty())
        {
            return NULL;
        }

#ifdef _MSC_VER
        FILE* file = NULL;
        if (fopen_s(&file, fileName.c_str(), "w") != 0)
        {
            file = NULL;
        }
#else
        FILE* file = fopen(fileName.c_str(), "w");
#endif
        return file;
    }

    //! Закрывает файл лога.
    void Close()
    {
        if ((m_file) && (m_file != stderr))
        {
            fclose(m_file);
        }
    }

    //! Пересохраняет файл лога, добавив суффикс ".1".
    //! @note Файл должен быть закрыт.
    void Backup()
    {
        if (m_fileName.empty())
        {
            return;
        }

        boost::filesystem::path path1(m_fileName);
        boost::filesystem::path path2(m_fileName + ".1");

        boost::filesystem::rename(path1, path2);
    }
};

//! Тип определяющий лог-синглтон.
typedef CSingletonHolder<CLog> CLogSingleton;

#endif // _INC_LOG_H
