//! @file Time.h
//! Объявление типов, относящихся к времени

#ifndef _INC_TIME_H
#define _INC_TIME_H

#include <stdint.h>

//#include <boost/date_time.hpp>

#include <vector>

namespace chart
{
//! Тип, представляющий системное время в микросекундах
typedef uint64_t TimeCyclic;

//! Длительность одной секунды в микросекундах
const TimeCyclic ONE_SECOND = 1000000;

//! Задает временнОй промежуток
struct TimeRange
{
    //! Конструктор.
    TimeRange::TimeRange() : timeStart(0), timeFinish(0)
    {
    }
    
    //! Конструктор.
    //! @param start  - [in] время начала промежутка;
    //! @param finish - [in] время конца промежутка;
    TimeRange::TimeRange(TimeCyclic start, TimeCyclic finish) : timeStart(start), timeFinish(finish)
    {
    }

    TimeCyclic timeStart;   //!< Начало промежутка
    TimeCyclic timeFinish;  //!< Конец промежутка
};

//! Контейнер для хранения временнЫх промежутков.
typedef std::vector<TimeRange> TimeRanges;

//#pragma warning(push)
//#pragma warning(disable : 4201) // "nonstandard extension used : nameless struct/union" warnings

//! Тип, представляющий московское время
typedef uint64_t TimeMoscow;

//#pragma warning(pop)

//! Тип контейнера для хранения списка московского времени.
typedef std::vector<TimeMoscow> MoscowTimeVector;

}

#endif // _INC_TIME_H
