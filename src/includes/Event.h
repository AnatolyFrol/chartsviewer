//! @file Event.h
//! Объявление класса CEvent.

#ifndef _EVENT_H
#define _EVENT_H

#include <stdint.h>

#include "Time.h"
//#include "includes/BinaryData.h"
#include "DataId.h"

#include <boost/shared_array.hpp>



namespace chart
{

//! Класс, преставляющий "событие".
class CEvent
{
public:
    CEvent();
    CEvent(DataId idData, boost::shared_array<uint8_t> data, uint32_t size,
           TimeCyclic timeCyclic, TimeMoscow timeMoscow);
    //CEvent(const CBinaryData& data);

    DataId GetDataId() const;
    boost::shared_array<uint8_t>       GetData();
    const boost::shared_array<uint8_t> GetData() const;
    uint32_t   GetSize() const;
    TimeCyclic GetCyclicTime() const;
    TimeMoscow GetMoscowTime() const;

private:
    TimeCyclic                      m_timeCyclic;
    TimeMoscow                      m_timeMoscow;

    boost::shared_array<uint8_t>    m_data;
    uint32_t                        m_size;

    DataId                   m_idData;
};

}

#endif // _INC_EVENT_H
