//! @file Format.h
//! Функция для форматирования строк

#ifndef _INC_FORMAT_H
#define _INC_FORMAT_H

#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <vector>
#include <string>

//! Форматирует строку по принципу функции printf().
//! @param format - [in] строка форматирования
//! @return Отформатированная строка 
inline std::string FormatString(const char* format, ...)
{
    va_list args;
    va_start(args, format);

#ifdef _MSC_VER
    int len = _vscprintf(format, args);
#else
    va_list args_copy;
    va_copy(args_copy, args);

    int len = vsnprintf(NULL, 0, format, args_copy);

    va_end(args_copy);
#endif

    if (len < 0)
    {
        va_end(args);
        return "[FORMAT STRING ERROR 1]";
    }
    std::vector<char> vecBuff(len + 2, 0);

    int cntWritten = -1;
#ifdef _MSC_VER
    cntWritten = vsnprintf_s(&vecBuff[0], vecBuff.size() - 1, _TRUNCATE, format, args);
#else
    cntWritten = vsnprintf(&vecBuff[0], vecBuff.size() - 1, format, args);
#endif
    if (cntWritten < 0)
    {
        va_end(args);
        return "[FORMAT STRING ERROR 2]";
    }

    va_end(args);

    return &vecBuff[0];
}

//! Форматирует строку по принципу функции vprintf().
//! @param format - [in] строка форматирования
//! @param args   - [in] список значений для вывода
//! @return Отформатированная строка 
inline std::string FormatStringV(const char* format, va_list args)
{
#ifdef _MSC_VER
    int len = _vscprintf(format, args);
#else
    va_list args_copy;
    va_copy(args_copy, args);

    int len = vsnprintf(NULL, 0, format, args_copy);

    va_end(args_copy);
#endif

    if (len < 0)
    {
        return "[FORMAT STRING ERROR 1]";
    }

    std::vector<char> vecBuff(len + 2, 0);

    int cntWritten = -1;
#ifdef _MSC_VER
    cntWritten = vsnprintf_s(&vecBuff[0], vecBuff.size() - 1, _TRUNCATE, format, args);
#else
    cntWritten = vsnprintf(&vecBuff[0], vecBuff.size() - 1, format, args);
#endif
    if (cntWritten < 0)
    {
        return "[FORMAT STRING ERROR 2]";
    }

    return &vecBuff[0];
}

#endif // _INC_FORMAT_H
