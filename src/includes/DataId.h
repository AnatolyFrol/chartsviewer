//! @file DataId.h
//! Объявление и реализация структуры DataId.

#ifndef _INC_DATA_ID_H
#define _INC_DATA_ID_H

#include <stdint.h>

#include <set>

namespace chart
{

#pragma pack(push)
#pragma pack(1)

#pragma warning(push)
#pragma warning(disable : 4201) // "nonstandard extension used : nameless struct/union" warnings

//! Структура, идентифицирующая данные
union DataId
{
    //! Конструктор по умолчанию.
    DataId(){};

    //! Конструктор.
    //! @param typeDt  - [in] тип данных;
    //! @param idDt    - [in] идентификатор данных;
    //! @param indexDt - [in] индекс.
    DataId(uint8_t typeDt, uint16_t idDt, uint8_t indexDt) :
        type(typeDt), id(idDt), index(indexDt)
    {}    

    struct
    {
        uint8_t  type;  //!< тип данных;
        uint8_t  index; //!< индекс;
        uint16_t id;    //!< идентификатор данных.
    };

    uint32_t uint32;

    //! Тип данных.
    enum Type
    {
        DT_REF_RASTER_FRAME = 0x00,
        DT_REF_VECTOR_FRAME = 0x01,
        DT_REF_AUDIO        = 0x02,
        DT_EVENT_PULSE      = 0x03,
        DT_EVENT_LONG       = 0x04,
        DT_PARAMS           = 0x05,
        DT_RASTER_FRAME     = 0x06,
        DT_VECTOR_FRAME     = 0x07,
        DT_AUDIO_BUFFER     = 0x08,
        DT_REFERENCE_EVENT  = 0x09
    };

    //! Определяет являются ли данные видеокадром.
    //! @return true - данные являются видеокадром, false - иначе.
    bool IsFrame() const
    {
        return ((type == DataId::DT_RASTER_FRAME) || (type == DataId::DT_VECTOR_FRAME));
    }

    //! Определяет являются ли данные звуковым буфером.
    //! @return true - данные являются звуковым буфером, false - иначе.
    bool IsAudio() const
    {
        return (type == DataId::DT_AUDIO_BUFFER);
    }

    //! Определяет являются ли данные событием.
    //! @return true - данные являются событием, false - иначе.
    bool IsEvent() const
    {
        return ((type == DataId::DT_EVENT_LONG) || (type == DataId::DT_EVENT_PULSE));
    }

    //! Определяет являются ли данные ссылкой.
    //! @return true - данные являются ссылкой, false - иначе.
    bool IsReference() const
    {
        return ((type == DataId::DT_REF_RASTER_FRAME) || 
                (type == DataId::DT_REF_VECTOR_FRAME) ||
                (type == DataId::DT_REF_AUDIO));
    }
};

#pragma warning(pop)

#pragma pack(pop)

typedef std::set<DataId> DataSet;

//! Оператор для сравнения двух объектов типа DataId.
//! @param idData1 - [in] идентификтор данных;
//! @param idData2 - [in] идентификтор данных.
//! @return true - если idData1 < idData2, false - иначе.
inline bool operator<(const DataId& idData1, const DataId& idData2)
{
    return (idData1.uint32 < idData2.uint32);
}

} // namespace spoi

#endif // _INC_DATA_ID_H
