//! @file Event.cpp
//! Реализация класса CEvent.

#include "Event.h"

namespace chart
{

//! Конструктор по умолчанию.
CEvent::CEvent() :
    m_size(0), m_timeCyclic(0)
{
    m_idData.uint32     = 0;
    m_timeMoscow        = 0;
}

//! Конструктор.
//! @param idData     - [in] идентификатор данных
//! @param data       - [in] буфер данных
//! @param size       - [in] размер буфера данных
//! @param timeCyclic - [in] метка системного времени, относящаяся к данным
//! @param timeMoscow - [in] метка московского времени, относящаяся к данным
CEvent::CEvent(DataId idData, boost::shared_array<uint8_t> data, uint32_t size,
               TimeCyclic timeCyclic, TimeMoscow timeMoscow) :
    m_idData(idData), m_data(data), m_size(size),
    m_timeCyclic(timeCyclic), m_timeMoscow(timeMoscow)
{
}


//! Возвращает идентификатор данных.
//! @return идентификатор данных
DataId CEvent::GetDataId() const
{
    return m_idData;
}

//! Возвращает буфер данных.
//! @return буфер данных
boost::shared_array<uint8_t> CEvent::GetData()
{
    return m_data;
}

//! Возвращает буфер данных.
//! @return буфер данных
const boost::shared_array<uint8_t> CEvent::GetData() const
{
    return m_data;
}

//! Возвращает размер буфера данных.
//! @return размер буфера данных
uint32_t CEvent::GetSize() const
{
    return m_size;
}

//! Возвращает метку системного времени, относящуюся к событию.
//! @return системное время
TimeCyclic CEvent::GetCyclicTime() const
{
    return m_timeCyclic;
}

//! Возвращает метку московского времени, относящуюся к событию.
//! @return московское время
TimeMoscow CEvent::GetMoscowTime() const
{
    return m_timeMoscow;
}

} // namespace spoi