#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void MySetupUi();

private:
    Ui::MainWindow *ui;

    int x;
    int y;
    //QPixmap m_pixmap;

protected:
void paintEvent(QPaintEvent *);
void mousePressEvent(QMouseEvent *);
};

#endif // MAINWINDOW_H
