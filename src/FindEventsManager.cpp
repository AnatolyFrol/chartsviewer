//! @file FindEventsManager.cpp
//! Реализация класса chart::CFindEventsManager.

#include "FindEventsManager.h"

#include <algorithm>

#include <boost/assign/std/vector.hpp>

namespace chart
{

//! Стартовая ширина окна = 500 пикселей.
const int32_t  START_WIDTH = 500;
//! Стартовый временной отрезок = 50 секунд.
const uint32_t START_DURATION = 50;

//! Конструктор.
//! @param pWidget - [in] главный виджет отображения поиска;
//! @param parent  - [in] родительское окно.
CFindEventsManager::CFindEventsManager(CFindEventsWidget* pWidget, QObject *parent/* = 0*/) :
    QObject(parent), m_coefIncrement(0.01), m_pFindWidget(pWidget), m_pTimeOperator(NULL),
    m_pBindingPoints(NULL)
{
}

//! Деструктор.
CFindEventsManager::~CFindEventsManager()
{
    if (m_pTimeOperator)
    {
        delete m_pTimeOperator;
    }
    if (m_pBindingPoints)
    {
        delete m_pBindingPoints;
    }
}

//! Инициализация записей списка "событий" и списка "кадров".
//! @param events - [in] список событий;
//! @param frames - [in] список кадров.
void CFindEventsManager::InitRecords(const std::vector<CEvent>& events, 
                                     const std::vector<CEvent>& frames)
{
    m_chartData.InitChartEvets(m_eventNames);
    m_chartData.InitChartFrames(m_frameNames);

    m_chartData.SetEventSignal(events);
    m_chartData.SetFrameSignal(frames);
}

//! Инициализация менеджера всеми необходимыми данными.
//! @param sessionTime       - [in] временной промежуток для текущей сессии;
//! @param pMoscowTimeVector - [in] список значений московского времени, (индекс == системные сек.).
void CFindEventsManager::InitRecordsParameters(TimeRange sessionTime,
                                               boost::shared_ptr<MoscowTimeVector> pMoscowTimeVector)
{
    // Пусть, например, стартовая ширина окна = 500 пикселей
    m_width = START_WIDTH;
    // Пусть, например, стартовый временной отрезок = 50 секунд
    double duration = ONE_SECOND * START_DURATION;
    if ((sessionTime.timeFinish - sessionTime.timeStart) < duration)
    {
        duration = sessionTime.timeFinish - sessionTime.timeStart;
    }
    m_scale = duration / (double)m_width;
    m_markedChannel.timeMark = 0;

    m_boundedTimeOperator.SetAllSpan(sessionTime.timeStart, sessionTime.timeFinish);
    m_boundedTimeOperator.VisibleScreen(duration);

    ComputeTimeRange();

    if (!m_pTimeOperator)
    {
        m_pTimeOperator = new CDisplayTimeOperator();

        QObject::connect(m_pFindWidget, SIGNAL(ChartHorizontalScrollBar(int)),
                         this, SLOT(OnUpdateHorizontScroll(int)));

        QObject::connect(m_pFindWidget, SIGNAL(SizeChanged(QSize)),
                         this, SLOT(OnUpdateSize(QSize)));

        QObject::connect(m_pFindWidget, SIGNAL(EvtTimeMarkChanged(QPoint)),
                         this, SLOT(OnTimeMarkChanged(QPoint)));

        QObject::connect(m_pFindWidget, SIGNAL(EvtTimeMarkShift(int)),
                         this, SLOT(OnTimeMarkShift(int)));

        QObject::connect(m_pFindWidget, SIGNAL(EvtChannelSelection(int)),
                         this, SLOT(OnChannelSelected(int)));

        QObject::connect(m_pFindWidget, SIGNAL(ZoomIn()),
                         this, SLOT(OnZoomIn()));

        QObject::connect(m_pFindWidget, SIGNAL(ZoomOut()),
                         this, SLOT(OnZoomOut()));

        QObject::connect(m_pFindWidget, SIGNAL(EvtChangeMode()),
                         this, SLOT(OnChangeMode()));
    }

    m_pTimeOperator->SetWorkSpan(m_boundedTimeOperator.GetCurrentTimeRange());
    m_pTimeOperator->SetWidth(m_width);
    m_pTimeOperator->SetMoscowTimeVector(pMoscowTimeVector);
    
    int height = CLinesPainter::bandHeight * (m_chartData.GetChannelCount() + 1);
    m_lines.SetWidth(QSize(m_width, height));
    RefreshCurrentChart();

    OnUpdateHorizontScroll(0);
}

//! Обновление текущего списка каналов для отображения.
void CFindEventsManager::RefreshCurrentChart()
{
    m_lines.SetTimeOperator(m_pTimeOperator);
    m_pFindWidget->SetTimeOperator(m_pTimeOperator);

    if (m_chartData.IsEventChart())
    {
        m_lines.SetIdList(m_eventNames);

        m_pFindWidget->DrawText(m_eventNames);
    }
    else
    {
        m_lines.SetIdList(m_frameNames);

        m_pFindWidget->DrawText(m_frameNames);
    }
}

//! "Приближение" к видимому (+).
void CFindEventsManager::OnZoomIn()
{
    TimeCyclic duration = m_boundedTimeOperator.GetDuration();

    double newScale = m_scale / 1.1;
    double dbDuration = (double)m_width * newScale;
    TimeCyclic oldDuration = duration;

    duration = dbDuration;
    // если меньше 1 секунды
    if (duration < ONE_SECOND)
    {
        //1 секунда
        duration = ONE_SECOND;
    }
    else if ((oldDuration - duration) < ONE_SECOND) 
    {
        // изменения меньше чем на 1 секунду
        duration = oldDuration - ONE_SECOND;
    }

    if (m_boundedTimeOperator.IsVisible(m_markedChannel.timeMark))
    {
        m_boundedTimeOperator.SetCenter(m_markedChannel.timeMark);
    }
    m_boundedTimeOperator.VisibleScreen(duration);

    dbDuration = m_boundedTimeOperator.GetDuration();
    m_scale = dbDuration / (double)m_width;

    ComputeTimeRange();
    UpdateChartView();
}

//! "удаление" от видимых объектов (-).
void CFindEventsManager::OnZoomOut()
{
    TimeCyclic duration = m_boundedTimeOperator.GetDuration();

    double newScale = m_scale * 1.1;
        
    double dbDuration = (double)m_width * newScale;

    //когда видимая область была очень маленькой и не поменялась
    if ((TimeCyclic)dbDuration == duration)
    {
        duration += ONE_SECOND;
    }
    else
    {
        duration = dbDuration;
    }

    if (m_boundedTimeOperator.IsVisible(m_markedChannel.timeMark))
    {
        m_boundedTimeOperator.SetCenter(m_markedChannel.timeMark);
    }
    m_boundedTimeOperator.VisibleScreen(duration);

    dbDuration = m_boundedTimeOperator.GetDuration();
    m_scale = dbDuration / (double)m_width;

    ComputeTimeRange();
    UpdateChartView();
}

//! Настройка скролбара при изменении масштаба.
void CFindEventsManager::ComputeTimeRange()
{
    int coef[3] = { 0, 0, 0 };
    m_boundedTimeOperator.ComputeTimeRange(coef);

    m_pFindWidget->SetHorizontalScroll(coef[0],  // max
                                       coef[1],  // step
                                       coef[2]); // value
}

//! Обновление позиции скрола пользователем.
//! @param position - [in] положение скрола.
void CFindEventsManager::OnUpdateHorizontScroll(int position)
{
    TimeCyclic duration = m_boundedTimeOperator.GetDuration();
        
    m_boundedTimeOperator.SetHorizontScroll(position);
    m_boundedTimeOperator.VisibleScreen(duration);
    UpdateChartView();
}

//! Изменение размера окна с временной диаграммой.
//! @param newSize - [in] новое значение размера окна.
void CFindEventsManager::OnUpdateSize(QSize newSize)
{
    TimeCyclic allDuration = m_boundedTimeOperator.GetAllDuration();
    double prevDuration = m_boundedTimeOperator.GetDuration();

    m_width = newSize.width();

    double duration = (double)m_width * m_scale;
    if (duration >= allDuration)
    {
        duration = allDuration;
        m_scale = (double)allDuration / (double)m_width;
    }
    // смещаем таким образом, чтобы стартовая точка не смещалась
    double deltaDuration = duration - prevDuration;
    m_boundedTimeOperator.ShiftCenter(deltaDuration / 2.0);
    m_boundedTimeOperator.VisibleScreen(duration);

    int height = CLinesPainter::bandHeight * (m_chartData.GetChannelCount() + 1);
    if (newSize.height() > height)
    {
        // уменьшение высоты, чтобы не возникало вертикальной полосы прокрутки
        height = newSize.height() - 7;
    }
    m_lines.SetWidth(QSize(m_width, height));
    m_pTimeOperator->SetWidth(m_width);
    m_pFindWidget->SetHeight(height);

    ComputeTimeRange();
    UpdateChartView();
}

//! Изменение из виджета.
//! @param position - [in] координата, включающая индекс канала (y) и метку на временной шкале (x).
void CFindEventsManager::OnTimeMarkChanged(QPoint position)
{
    m_markedChannel.timeMark = m_pTimeOperator->GetTimeFromScreen(position.x());
    m_markedChannel.xPosition = position.x();

    int prevIndex = m_markedChannel.channelIndex;
    int channelIndex = position.y();
    if (channelIndex >= m_chartData.GetChannelCount())
    {
        m_markedChannel.channelIndex = -1;
    }
    else
    {
        m_markedChannel.channelCode = m_chartData.GetCodeByIndex(channelIndex);
        m_markedChannel.channelIndex = channelIndex;
    }

    if (prevIndex != m_markedChannel.channelIndex)
    {
        UpdateChartView();
    }

    UpdateChannelButtons();
}

//! Выбор канала из списка имен из левой панели.
//! @param channelIndex - [in] индекс выбранного канала.
void CFindEventsManager::OnChannelSelected(int channelIndex)
{
    if (channelIndex == m_markedChannel.channelIndex)
    {
        return;
    }

    int prevIndex = m_markedChannel.channelIndex;

    if (channelIndex >= m_chartData.GetChannelCount())
    {
        m_markedChannel.channelIndex = -1;
    }
    else
    {
        m_markedChannel.channelCode = m_chartData.GetCodeByIndex(channelIndex);
        m_markedChannel.channelIndex = channelIndex;
    }

    if (prevIndex != m_markedChannel.channelIndex)
    {
        UpdateChartView();
    }

    UpdateChannelButtons();
}

//! Обновление кнопок "вперед" и "назад" для каждого канала
void CFindEventsManager::UpdateChannelButtons()
{
    if (m_markedChannel.channelIndex >= 0)
    {
        ChannelRecords channel = m_chartData.GetChannelByIndex(m_markedChannel.channelIndex);

        if (channel.size() == 0)
        {
            m_markedChannel.leftShift  = false;
            m_markedChannel.rightShift = false;
        }
        else
        {
            ChannelRecords::iterator endPoint = channel.end();
            endPoint--;
            m_markedChannel.leftShift = (m_markedChannel.timeMark <= channel.begin()->GetCyclicTime()) 
                                      ? false 
                                      : true;
            // если последняя точка - конец длительного сигнала сместимся еще левее
            if (CGroupOfChart::DataIdToEchannelState(endPoint->GetDataId()) == CS_LOW)
            {
                endPoint--;
            }
            m_markedChannel.rightShift = (m_markedChannel.timeMark >= endPoint->GetCyclicTime()) 
                                       ? false
                                       : true;
        }
    }
    m_pFindWidget->SetTimeMark(m_markedChannel);
}

//! Нахождение ближайшей метки слева.
//! @param channel  - [in] канал;
//! @param timeMark - [in] стартовое время для поиска.
//! @return указатель на найденный сигнал.
ChannelRecords::const_iterator LeftSide(const ChannelRecords& channel, TimeCyclic timeMark)
{
    ChannelRecords::const_iterator itlow = std::find_if(channel.begin(), channel.end(), 
                                                        CEventTimeLargerOrEquel(timeMark));
    if (itlow == channel.end())
    {
        itlow--;
    }
    else if (itlow->GetCyclicTime() < timeMark)
    {
        ;
    }
    else if (itlow != channel.begin())
    {
        itlow--;
    }
    else
    {
        itlow = channel.begin();
    }

    return itlow;
}

//! Нахождение ближайшей метки справа.
//! @param channel    - [in] канал;
//! @param timeMark - [in] стартовое время для поиска.
//! @return указатель на найденный сигнал.
ChannelRecords::const_iterator RightSide(const ChannelRecords& channel, TimeCyclic timeMark)
{
    ChannelRecords::const_iterator itup = std::find_if(channel.begin(), channel.end(),
                                                        CEventTimeLarger(timeMark));

    if (itup == channel.end())
    {
        ;
    }
    else if (itup->GetCyclicTime() > timeMark)
    {
        ;
    }
    else if (itup != channel.end())
    {
        itup++;
    }
    else
    {
        // последняя точка у канала
        itup = channel.end();
    }

    return itup;
}

//! Смещение метки влево или вправо.
//! @param channel  - [in] канал;
//! @param timeMark - [in] стартовое время метки;
//! @param shift    - [in] направление смещения.
//! @return указатель на найденный сигнал.
ChannelRecords::const_iterator ShiftMark(const ChannelRecords& channel, TimeCyclic timeMark, 
                                         int shift)
{
    ChannelRecords::const_iterator markPoint = channel.begin();

    if (shift < 0)
    {
        markPoint = LeftSide(channel, timeMark);
    }
    else
    {
        markPoint = RightSide(channel, timeMark);

        if (markPoint == channel.end())
        {
            markPoint--;
        }
    }

    return markPoint;
}

//! Сдвиг метки, вместе с ней сдвигается окно.
//! @param shift - [in] направление смещения.
void CFindEventsManager::OnTimeMarkShift(int shift)
{
    ChannelRecords channel = m_chartData.GetChannelByIndex(m_markedChannel.channelIndex);

    ChannelRecords::const_iterator markPoint = ShiftMark(channel, m_markedChannel.timeMark, shift);
    // если точка является концом длит. события
    if (CGroupOfChart::DataIdToEchannelState(markPoint->GetDataId()) == CS_LOW)
    {
        markPoint = ShiftMark(channel, markPoint->GetCyclicTime(), shift);
    }

    m_markedChannel.timeMark = markPoint->GetCyclicTime();

    m_boundedTimeOperator.SetCenter(m_markedChannel.timeMark);
        
    m_boundedTimeOperator.VisibleScreen(m_pTimeOperator->GetVisibleDuration());

    //корректировка скроллбара
    ComputeTimeRange();
    //перерисовка окна, также перерсовка метки и обновление параметров времени m_pTimeOperator
    UpdateChartView();
    
    UpdateChannelButtons();
}

//! Отрисовка объектов в видимой области.
void CFindEventsManager::UpdateChartView()
{
    m_lines.Clear();
    TimeRange currentTimeRange = m_boundedTimeOperator.GetCurrentTimeRange();
    m_pTimeOperator->SetWorkSpan(currentTimeRange);

    if (m_pBindingPoints == 0)
    {
        m_pBindingPoints = new std::vector<int>();
        m_pFindWidget->BindingPoints(m_pBindingPoints);
    }
    else
    {
        m_pBindingPoints->clear();
    }
    
    m_lines.SelectChannel(m_markedChannel.channelIndex);

    for (int channelIndex = 0; channelIndex < m_chartData.GetChannelCount(); channelIndex++)
    {
        const ChannelRecords& channel = m_chartData.GetChannelByIndex(channelIndex);

        // точек нет - продолжить
        if (channel.size() == 0)
        {
            continue;
        }

        //найдем ближайшую точку слева
        ChannelRecords::const_iterator startPoint = LeftSide(channel, currentTimeRange.timeStart);
        //найдем ближайшую точку справа
        ChannelRecords::const_iterator endPoint = RightSide(channel, currentTimeRange.timeFinish);

        for (ChannelRecords::const_iterator it = startPoint; it != endPoint; it++)
        {
            m_lines.AddPoint(it->GetCyclicTime(), m_chartData.GetCodeByIndex(channelIndex), 
                             CGroupOfChart::DataIdToEchannelState(it->GetDataId()));
            if ((channelIndex == m_markedChannel.channelIndex) && 
                (m_pTimeOperator->IsVisible(it->GetCyclicTime(), CS_PULSE)))
            {
                m_pBindingPoints->push_back(m_pTimeOperator->GetXCoordinate(it->GetCyclicTime(), CS_PULSE));
            }
        }
    }

    m_pFindWidget->UpdatePixmap(m_lines.GetPixmap());

    // положение метки в окне тоже меняется
    m_markedChannel.xPosition = 
        m_pTimeOperator->TranslateTimeToXCoordinate(m_markedChannel.timeMark);
    m_pFindWidget->UpdateScreen(m_markedChannel);//обновление параметров окна в View части
}

//! Смена отображаемого окна (режима).
void CFindEventsManager::OnChangeMode()
{
    m_chartData.SelectNext();

    RefreshCurrentChart();

    m_markedChannel.channelIndex = -1;

    //корректировка скроллбара
    ComputeTimeRange();
    //перерисовка окна, также перерсовка метки и обновление параметров времени m_pTimeOperator
    UpdateChartView();  
    UpdateChannelButtons();
}

//! Определение списка свойств сигналов каналов для каждого режима.
//! @param eventNames - [in] список имен событий.
//! @param frameNames - [in] список имен кадров.
//! @return список свойств сигналов каналов.
void CFindEventsManager::SetConfiguredDataId(std::vector<std::pair<DataId, std::string>> eventNames,
                                             std::vector<std::pair<DataId, std::string>> frameNames)
{
    m_eventNames = eventNames;
    m_frameNames = frameNames;
}

//! Установка вида отображаемого времени (московское или системное).
//! @param isMoscowTimeVector - [in] true - отображается московское время, false - системное время.
void CFindEventsManager::SetVisibleTimeMode(bool isMoscowTime)
{
    if (m_pTimeOperator)
    {
        m_pTimeOperator->SetVisibleTimeMode(isMoscowTime);
    }
}

//! Установка позиции метки в окне поиска.
//! @param markedTime - [in] системное время метки.
void CFindEventsManager::SetMarkedTime(TimeCyclic markedTime)
{
    RefreshCurrentChart();

    m_markedChannel.channelIndex = -1;
    m_markedChannel.timeMark = markedTime;

    m_boundedTimeOperator.SetCenter(m_markedChannel.timeMark);

    m_boundedTimeOperator.VisibleScreen(m_pTimeOperator->GetVisibleDuration());

    //корректировка скроллбара
    ComputeTimeRange();
    //перерисовка окна, также перерсовка метки и обновление параметров времени m_pTimeOperator
    UpdateChartView();

    UpdateChannelButtons();
}

//! Получение позиции метки на временной шкале.
//! @return системное время метки.
TimeCyclic CFindEventsManager::GetMarkedTime()
{
    return m_markedChannel.timeMark;
}

}