//! @file FindEventsManager.h
//! Объявление класса chart::CFindEventsManager.

#ifndef FINDEVENTSMANAGER_H
#define FINDEVENTSMANAGER_H

#include <QtCore>
#include <QtGui>

#include "FindEventsWidget.h"
#include "ChartWidget.h"


namespace chart
{
    //! Структура, предназначенная для нахождения CEvent в списке, с временем большим заданному.
    struct CEventTimeLarger
    {
        CEventTimeLarger(TimeCyclic tm) : time(tm)
        { }
        bool operator() (const CEvent& ev) const
        {
            return ev.GetCyclicTime() > time;
        }
    private:
        TimeCyclic time;
    };

    //! Структура, предназначенная для нахождения CEvent, с временем большим или равным заданному.
    struct CEventTimeLargerOrEquel
    {
        CEventTimeLargerOrEquel(TimeCyclic tm) : time(tm)
        { }
        bool operator() (const CEvent& ev) const
        {
            return ev.GetCyclicTime() >= time;
        }
    private:
        TimeCyclic time;
    };

//! Класс управления окном поиска.
class CFindEventsManager : public QObject
{
    Q_OBJECT
public:
    CFindEventsManager(CFindEventsWidget* pWidget, QObject *parent = 0);
    ~CFindEventsManager();
    void InitRecordsParameters(TimeRange sessionTime,
                               boost::shared_ptr<MoscowTimeVector> pMoscowTimeVector);
    void InitRecords(const std::vector<CEvent>& events, const std::vector<CEvent>& frames);

    void SetVisibleTimeMode(bool isMoscowTime);
    void SetMarkedTime(TimeCyclic markedTime);
    void SetConfiguredDataId(std::vector<std::pair<DataId, std::string>> eventNames,
                             std::vector<std::pair<DataId, std::string>> frameNames);
    TimeCyclic GetMarkedTime();

public slots:
    void OnUpdateSize(QSize newSize);

private:
    CFindEventsWidget*    m_pFindWidget;

    int32_t               m_width;
    // отношение = pixel / TimeCyliс
    double                m_scale;
    //коэфициент смещения относительно m_currentTimeRange
    //отношение смещения при единичном нажатии влево/вправо
    const double          m_coefIncrement;

    uint64_t              m_coefConversion;

    CGroupOfChart         m_chartData;
    ChannelMarkProperty   m_markedChannel;

    CLinesPainter         m_lines;

    CBoundedTimeOperator  m_boundedTimeOperator;
    CDisplayTimeOperator* m_pTimeOperator;
    std::vector<int>*     m_pBindingPoints;

    std::vector<std::pair<DataId, std::string>> m_eventNames;
    std::vector<std::pair<DataId, std::string>> m_frameNames;
    
private:
    void UpdateChartView();
    void ComputeTimeRange();
    void RefreshCurrentChart();
    void UpdateChannelButtons();
    
private slots:
    void OnUpdateHorizontScroll(int position);
    void OnTimeMarkChanged(QPoint position);
    void OnChannelSelected(int channelIndex);
    void OnTimeMarkShift(int shift);
    void OnZoomIn();
    void OnZoomOut();
    void OnChangeMode();
};

}

#endif // FINDEVENTSMANAGER_H
