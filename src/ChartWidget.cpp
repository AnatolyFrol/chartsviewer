//! @file ChartWidget.cpp
//! Реализация классов chart::CShortEventSettings, chart::CGroupOfChart, chart::CChartWidget,
//! chart::CBoundedTimeOperator, chart::CDisplayTimeOperator, chart::CLinesPainter 
//! также enum chart::EChannelState, struct chart::NamedDataId, struct chart::ChannelMarkProperty

#include "ChartWidget.h"

#include <QLabel>
#include <QGridLayout>
#include <QGraphicsLineItem>
#include <QMouseEvent>
#include <qcolor.h>

#include <boost/assign/std/vector.hpp>
#include <boost/assign/list_of.hpp>

#include "..\includes\DataId.h"

namespace chart
{
    const QColor CHANNEL_BORDER_COLOR          = Qt::gray;
    const QColor TIME_MARK_COLOR               = Qt::red;
    const QColor SELECTION_COLOR               = Qt::cyan;
    const QColor PULSE_COLOR                   = Qt::red;
    const std::vector<QColor> FORBIDDEN_COLORS = boost::assign::list_of(SELECTION_COLOR)(PULSE_COLOR);
    

//! Конструктор.
//! @param parent - [in] родительское окно;
CChartWidget::CChartWidget(QWidget *parent) : QGraphicsView(parent), m_pPixmapItem(NULL),
    m_pVerticalLine(NULL), m_pTimeMarkLine(NULL), m_pBindingPoints(NULL)
{
    setMouseTracking(true);

    m_pScene = new QGraphicsScene(this);
    setScene(m_pScene);
}

//! Событие - движение мышкой.
//! @param pEvent - [in] объект-событие.
void CChartWidget::mouseMoveEvent(QMouseEvent *pEvent)
{
    if (m_pVerticalLine)
    {
        int posX = pEvent->x();

        if (m_pBindingPoints != 0 && m_pBindingPoints->size() > 0)
        {
            std::vector<int>::iterator startPoint = m_pBindingPoints->begin();
            // указатель на последний элемент
            std::vector<int>::iterator endPoint = m_pBindingPoints->end() - 1; 
            std::vector<int>::iterator itlow = std::lower_bound(startPoint, endPoint, posX);
            std::vector<int>::iterator itup = itlow;

            if ((*itlow) > posX && (itlow > startPoint))
            {
                itlow--;
            }

            int deltaLow = std::abs((*itlow) - posX);
            int deltaUp = std::abs((*itup) - posX);
            // определяем расстояние до ближайшей
            if ((deltaLow <= deltaUp) && (deltaLow < 30))
            {
                posX = (*itlow);
            }
            else if ((deltaLow > deltaUp) && (deltaUp < 30))
            {
                posX = (*itup);
            }
        }

        m_pVerticalLine->setPos(posX, m_pVerticalLine->pos().y());
        emit EvtTempTimePosition(posX);
    }
}

//! Установка высоты окна.
//! @param height - [in] высота окна.
void CChartWidget::SetHeight(int height)
{
    if (m_pVerticalLine)
    {
        m_pVerticalLine->setLine(0, 0, 0, height);
    }
    if (m_pTimeMarkLine && m_pTimeMarkLine->isActive())
    {
        m_pTimeMarkLine->setLine(m_pTimeMarkLine->pos().x(), 0, m_pTimeMarkLine->pos().x(), height);
    }
}

//! Событие - нажатие на график на экране
//! @param pEvent - [in] объект-событие.
void CChartWidget::mousePressEvent(QMouseEvent *pEvent)
{
    QPoint timeMarkPos = pEvent->pos();
    timeMarkPos.setX(m_pVerticalLine->pos().x());
    SetTimeMark(timeMarkPos.x());
    emit TimeMarkChanged(timeMarkPos);
}

//! Установка временной метки.
//! @param position - [in] новая координата у метки.
void CChartWidget::SetTimeMark(int position)
{
    if (m_pTimeMarkLine)
    {
        int h = m_pScene->height();
        m_pTimeMarkLine->setLine(position, 0, position, h);
    }
    else
    {
        QPen pen(TIME_MARK_COLOR, 2);
        m_pTimeMarkLine = m_pScene->addLine(position, 0, position, m_pScene->height(), pen);
    }
    m_pTimeMarkLine->show();
}

//! Скрытие метки
void CChartWidget::HideTimeMark()
{
    if (m_pTimeMarkLine)
    {
        m_pTimeMarkLine->hide();
    }
}

//! Установка картинки в виджете поиска.
//! @param pixmap - [in] картинка с графиками.
void CChartWidget::SetPixmap(QPixmap pixmap)
{
    QPen pen(Qt::gray, 1);
    pen.setStyle(Qt::DashLine);

    if (pixmap.isNull())
    {
        //Пусть, например, изначальный размер экрана (400, 200)
        pixmap = QPixmap(QSize(400, 200));
        pixmap.fill();
    }

    if (!pixmap.isNull())
    {
        if (!m_pPixmapItem)
        {
            m_pPixmapItem = m_pScene->addPixmap(pixmap);
            m_pVerticalLine = m_pScene->addLine(0, 0, 0, pixmap.height(), pen);
            setScene(m_pScene);
        }
        else
        {
            m_pPixmapItem->setPixmap(pixmap);
        }
    }

    m_pScene->setSceneRect(pixmap.rect());

    update();
}

//! Установка точек-привязок.
//! @param pPoints - [in] список вертикальных координат для привязки.
void CChartWidget::BindingPoints(std::vector<int> *pPoints)
{
    m_pBindingPoints = pPoints;
}

//! Конструктор.
CLinesPainter::CLinesPainter() : m_yLastIndex(0), m_painter(NULL)
{
}

//! Деструктор.
CLinesPainter::~CLinesPainter()
{
}

//! Установка указателя на временной оператор.
//! @param pTimeOperator - [in] Указатель на оператор.
void CLinesPainter::SetTimeOperator(CDisplayTimeOperator *pTimeOperator)
{
    m_pTimeOperator = pTimeOperator;
}

//! Установка габаритов.
//! @param size - [in] Габариты.
void CLinesPainter::SetWidth(QSize size)
{
    //ширина окна pixmap
    m_width = size.width();

    m_pixmap = QPixmap(size);

    m_pixmap.fill(QColor("white"));
}

//! Выбор канала.
//! @param indexSelected - [in] индекс канала.
void CLinesPainter::SelectChannel(int indexSelected)
{
    if (m_painter == NULL)
    {
        m_painter = new QPainter(&m_pixmap);
    }

    if (indexSelected >= 0)
    {
        m_painter->fillRect(0, indexSelected * bandHeight, m_width, bandHeight, SELECTION_COLOR);
    }
}

//! Запрещен ли цвет к использованию.
//! @param red   - [in] красная составляющая цвета;
//! @param green - [in] зеленая составляющая цвета;
//! @param blue  - [in] синяя составляющая цвета;
//! @return true - используемый цвет запрещен к использованию, false - не является запрещенным.
bool IsColorForbiden(int32_t red, int32_t green, int32_t blue)
{
    for (std::vector<QColor>::const_iterator color = FORBIDDEN_COLORS.begin(); 
                                             color < FORBIDDEN_COLORS.end(); ++color)
    {
        if ((abs(color->red()   - red  ) < 20) &&
            (abs(color->green() - green) < 20) &&
            (abs(color->blue()  - blue ) < 20))
        {
            return true;
        }
    }
    return false;
}

//! Извлечение случайного темного цвета, кроме "запрещенных".
//! @return цвет.
QColor GetRandColor()
{
    bool isSeachColor = true;
    QColor color;
    while (isSeachColor)
    {
        int32_t red   = std::rand() * 255 / RAND_MAX;
        int32_t green = std::rand() * 255 / RAND_MAX;
        int32_t blue  = std::rand() * 255 / RAND_MAX;

        if ( ((red + green + blue) / 3 < 160) &&
            !IsColorForbiden(red, green, blue) )
        {
            isSeachColor = false;
            color = QColor(red, green, blue);
        }
    }

    return color;
}

//! Добавление точки для отображения + рисование
//! @param time - [in] время;
//! @param dataId - [in] код точки;
//! @param state - [in] состояние 0/1;
void CLinesPainter::AddPoint(TimeCyclic time, uint32_t dataId, EChannelState state)
{
    // если первое использование рисовальщика
    if (m_painter == NULL)
    {
        m_painter = new QPainter(&m_pixmap);
    }

    if (!m_pTimeOperator->IsVisible(time, state))
    {
        return;
    }
    // если нет в заданном списке DataId
    if (m_lastPoint.count(dataId) == 0)
    {
        if (state != CS_PULSE)
        {
            QColor cl = GetRandColor();
            boost::shared_ptr<PointOfLine> value(new PointOfLine(0, m_yLastIndex, CS_LOW, time, cl));
            m_lastPoint[dataId] = value;
        }
        else
        {
            boost::shared_ptr<PointOfLine> value(new PointOfLine(0, m_yLastIndex, CS_LOW, time));
            m_lastPoint[dataId] = value;
        }
        m_yLastIndex++;
    }

    uint64_t x = m_pTimeOperator->GetXCoordinate(time, state);

    QPen pen(PULSE_COLOR, 1);
    m_painter->setPen(pen);

    const PointOfLine* pnt1 = m_lastPoint[dataId].get();
    PointOfLine* pnt2 = new PointOfLine(x, m_lastPoint[dataId]->yIndex, state, time, 
                                        m_lastPoint[dataId]->color);

    // Рисование сигналов
    PaintRect(pnt1, pnt2);

    m_lastPoint[dataId] = boost::shared_ptr<PointOfLine>( pnt2 );
}

//! Получение картинки.
//! @return картинка.
QPixmap CLinesPainter::GetPixmap()
{
    QPen channelPen(PULSE_COLOR, 1);
    QPen borderPen(CHANNEL_BORDER_COLOR, 1);

    for (ChannelPoint::iterator it = m_lastPoint.begin(); it != m_lastPoint.end(); it++)
    {
        // дорисовка длительных событий до правого края
        PaintRect(it->second.get(), NULL);
        m_painter->setPen(borderPen);
        m_painter->drawLine(0, (it->second->yIndex + 1) * bandHeight - 2, m_width, 
                            (it->second->yIndex + 1) * bandHeight - 2);
    }

    PaintScale();

    delete m_painter;
    m_painter = NULL;

    return m_pixmap;
}

//! Рисование прямоугольника по двум точкам.
//! @param pnt1 - [in] Левая точка;
//! @param pnt2 - [in] Правая точка (может отсутствовать).
void CLinesPainter::PaintRect(const PointOfLine* pnt1, const PointOfLine* pnt2)
{
    if (m_painter == NULL)
    {
        m_painter = new QPainter(&m_pixmap);
    }

    QPen channelPen(pnt1->color, 1);
    m_painter->setPen(channelPen);

    qreal leftX = pnt1->X;
    if ((pnt1->X == 0) && (pnt1->state == CS_HIGH))
    {
        leftX = -1.0;
    }

    if (pnt2 != NULL && pnt2->state == CS_PULSE) // событие 
    {
        qreal rightX = pnt2->X;
        m_painter->drawRect(rightX, pnt2->yIndex * bandHeight + yPadding, 2, channelHeight);
    }
    else if ((pnt1->state == CS_HIGH) && (pnt2 != NULL) && (pnt2->state == CS_LOW))  
    {
        // заканчивается выделенный временной участок
        qreal rightX = pnt2->X;
        QString timeText = "  " + m_pTimeOperator->CreateTextFromTime(pnt1->time);
        QRectF rect = QRectF(leftX, pnt1->yIndex * bandHeight + yPadding, rightX - leftX, 
            channelHeight);
        if ((rightX - leftX) > (timeText.length() * 6))
        {
            m_painter->drawRect(rect);
            m_painter->drawText(rect, Qt::AlignLeft, timeText);
        }
        else
        {
            m_painter->drawRect(rect);
        }
    }
    else if ((pnt1->state == CS_HIGH) && (pnt2 == NULL))
    {
        // дорисовка длит. событий
        QString timeText = "  " + m_pTimeOperator->CreateTextFromTime(pnt1->time);
        QRectF rect = QRectF(leftX, pnt1->yIndex * bandHeight + yPadding, m_width - leftX, 
                                channelHeight);
        if ((m_width - leftX) > (timeText.length() * 6))
        {
            m_painter->drawRect(rect);
            m_painter->drawText(rect, Qt::AlignLeft, timeText);
        }
        else
        {
            m_painter->drawRect(rect);
        }
    }
}

//! Определение временной дельты при отображении на шкале.
//! @param duration - [in] длительность отображаемого временного промежутка;
//! @param width    - [in] ширина окна.
//! @return         - ширина одного отсчета на шкале.
TimeCyclic CLinesPainter::DefineTimeDelta(uint64_t duration, uint32_t width)
{
    TimeCyclic deltaTime = ONE_SECOND;

    if (duration < ONE_SECOND)
    {
        deltaTime = ONE_SECOND;
    }
    else if (duration < (60 * ONE_SECOND))
    {
        if ((width / (duration / ONE_SECOND)) > CLinesPainter::scaleTextWidth)
        {
            deltaTime = ONE_SECOND;
        }
        else
        {
            deltaTime = ONE_SECOND * 10;
        }
    }
    else if (duration < (3600 * ONE_SECOND))
    {
        if ((width / (duration / (10 * ONE_SECOND))) > CLinesPainter::scaleTextWidth)
        {
            deltaTime = ONE_SECOND * 10;
        }
        else if ((width / (duration / (60 * ONE_SECOND))) > CLinesPainter::scaleTextWidth)
        {
            deltaTime = ONE_SECOND * 60;
        }
        else
        {
            deltaTime = ONE_SECOND * 600;
        }
    }
    else if (duration < (24 * 3600 * ONE_SECOND))
    {
        if ((width / (duration / (60 * ONE_SECOND))) > CLinesPainter::scaleTextWidth)
        {
            deltaTime = ONE_SECOND * 60;
        }
        else if ((width / (duration / (600 * ONE_SECOND))) > CLinesPainter::scaleTextWidth)
        {
            deltaTime = ONE_SECOND * 600;
        }
        else
        {
            deltaTime = ONE_SECOND * 3600;
        }
    }
    else
    {
        deltaTime = ONE_SECOND * 36000;
    }

    return deltaTime;
}

//! Рисование временной шкалы внизу графиков
void CLinesPainter::PaintScale()
{
    QPen borderPen(CHANNEL_BORDER_COLOR, 1);
    QFont font = QFont("Arial", 10, QFont::Black);
    m_painter->setFont(font);

    int yMark = m_pixmap.height();
    int lineCount = scaleTextWidth / scaleMarkWidth;

    int deltaMark = scaleMarkWidth;
    TimeCyclic deltaTime = DefineTimeDelta(m_pTimeOperator->GetVisibleDuration(), m_width);
    TimeCyclic startTime = m_pTimeOperator->GetVisibleRange().timeStart;
    TimeCyclic finishTime = m_pTimeOperator->GetVisibleRange().timeFinish;
    TimeCyclic startLabel = startTime / deltaTime;

    for (TimeCyclic i = startLabel; i < finishTime; i += deltaTime)
    {
        for (TimeCyclic j = deltaTime / 10; j < deltaTime; j += (deltaTime / 10))
        {
            if ((i + j) >= startTime && (i + j) <= finishTime)
            {
                int32_t x = m_pTimeOperator->TranslateTimeToXCoordinate(i + j);
                m_painter->drawLine(x, yMark, x, yMark - yPadding);
            }
        }
        if (i >= startTime)
        {
            int32_t x = m_pTimeOperator->TranslateTimeToXCoordinate(i);
            m_painter->drawLine(x, yMark, x, yMark - yPadding - yPadding);

            QString currentTime = m_pTimeOperator->CreateTextFromTime(i);
            QRectF textCoord(x + 1, yMark - channelHeight - yPadding - 1, scaleTextWidth,
                             channelHeight);
            m_painter->drawText(textCoord, Qt::AlignLeft | Qt::AlignVCenter, currentTime);
        }
    }
}

//! Инициализация списка каналов.
//! @param idList - [in] Список событий в виджете.
void CLinesPainter::SetIdList(std::vector<std::pair<DataId, std::string>> idList)
{
    if (m_lastPoint.size() > 0)
    {
        m_lastPoint.clear();
        m_yLastIndex = 0;
    }

    for (int i = 0; i < idList.size(); i++)
    {
        if (idList[i].first.type != DataId::DT_EVENT_LONG)
        {
            //time = 0 - мин. допустимое время (перепишется)
            boost::shared_ptr<PointOfLine> value(new PointOfLine(0, m_yLastIndex, CS_LOW, 0));
            m_lastPoint[idList[i].first.uint32] = value;
        }
        else
        {
            //time = 0 - мин. допустимое время (перепишется)
            QColor cl = GetRandColor();
            boost::shared_ptr<PointOfLine> value(new PointOfLine(0, m_yLastIndex, CS_LOW, 0, cl));
            m_lastPoint[idList[i].first.uint32] = value;
        }
        m_yLastIndex++;
    }
}

//! Очистка текущей картинки
void CLinesPainter::Clear()
{
    for (ChannelPoint::iterator it = m_lastPoint.begin(); it != m_lastPoint.end(); it++)
    {
        it->second->state = CS_LOW;
        // самая крайняя точка слева
        it->second->X = 0;
    }
    m_pixmap.fill();
}

//! Установка текущего временного диапазона.
//! @param workSpan - [in] временной диапазон.
void CDisplayTimeOperator::SetWorkSpan(TimeRange workSpan)
{
    m_startTime = workSpan.timeStart;
    m_finishTime = workSpan.timeFinish;
    //длительность отрезка
    m_duration = m_finishTime - m_startTime;
}

//! Определение ширины окна в пикселях.
//! @param width - [in] ширина.
void CDisplayTimeOperator::SetWidth(int width)
{
    m_width = width;
}

//! Видно ли на временном отрезке заданое время.
//! @param time  - [in] заданное время;
//! @param state - [in] состояние сигнала.
bool CDisplayTimeOperator::IsVisible(TimeCyclic time, EChannelState state)
{
    if (time < m_startTime && state == CS_HIGH)
    {
        return true;
    }
    else if (time > m_finishTime && state == CS_LOW)
    {
        return true;
    }
    else if (time < m_startTime || time > m_finishTime)
    {
        return false;
    }
    return true;
}

//! Определение координаты точки как части сигнала, по ее времени и состоянию.
//! @param time - [in] время;
//! @param state - [in] состояние точки.
//! @return горизонтальная координата в окне виджета.
uint64_t CDisplayTimeOperator::GetXCoordinate(TimeCyclic time, EChannelState state)
{
    TimeCyclic paintTime = time;
    if (time < m_startTime && state == CS_HIGH)
    {
        paintTime = m_startTime;
    }
    else if (time > m_finishTime && state == CS_LOW)
    {
        paintTime = m_finishTime;
    }

    uint64_t x = m_width * (paintTime - m_startTime) / m_duration;
    return x;
}

//! Определение координаты точки поее времени, если не укладывается в текущ. отрезок = -1
//! @param time - [in] время;
//! @return горизонтальная координата в окне виджета.
int CDisplayTimeOperator::TranslateTimeToXCoordinate(TimeCyclic time)
{
    if (time < m_startTime)
    {
        return -1;
    }
    else if (time > m_finishTime)
    {
        return -1;
    }

    uint64_t x = m_width * (time - m_startTime) / m_duration;
    return x;
}

//! Возвращает диапазон текущего временного отрезка.
//! @return временной диапазон.
TimeCyclic CDisplayTimeOperator::GetVisibleDuration()
{
    return m_duration;
}
//! Возвращает временной промежуток текущего временного отрезка.
//! @return временной промежуток.
TimeRange CDisplayTimeOperator::GetVisibleRange()
{
    return TimeRange(m_startTime, m_finishTime);
}

//! Возвращает соответствующее горизонтальной координате время.
//! @param x - [in] горизонтальная координата;
//! @return время.
TimeCyclic CDisplayTimeOperator::GetTimeFromScreen(int x)
{
    return (((uint64_t)x * m_duration) / m_width) + m_startTime;
}

//! Возвращает соответстующее горизонтальной координате текст с временем.
//! @param x - [in] горизонтальная координата;
//! @return строка с временем.
QString CDisplayTimeOperator::GetTimeTextFromScreen(int x)
{
    return CreateTextFromTime(GetTimeFromScreen(x));
}

//! Создание строки с временем.
//! @param time - [in] время.
//! @return строка формата ЧЧ:ММ:СС.
QString CDisplayTimeOperator::CreateTextFromTime(TimeCyclic time)
{
    if (m_isMoscowTime)
    {
        double idx = static_cast<double>(time) / static_cast<double>(ONE_SECOND);
        QString str("0:0:0");
        if (m_pMoscowTimeVector && (idx <= m_pMoscowTimeVector->size()))
        {
            TimeMoscow time = (*m_pMoscowTimeVector)[idx];
            uint16_t seconds = time % 60;
            uint16_t minutes = time / 60 % 60;
            uint16_t hours   = time / 3600 % 60;
            QString qstr = QString("%1:%2:%3").arg(hours,   2, 10, QChar('0'))
                                              .arg(minutes, 2, 10, QChar('0'))
                                              .arg(seconds, 2, 10, QChar('0'));
        }
        return str;
    }
    else
    {
        uint64_t secondsAll = std::floor(static_cast<double>(time) / 
                                         static_cast<double>(ONE_SECOND) + 0.5);

        uint16_t seconds = secondsAll % 60;
        uint16_t minutes = secondsAll / 60 % 60;
        uint16_t hours   = secondsAll / 3600 % 60;
        QString qstr = QString("%1:%2:%3").arg(hours,   2, 10, QChar('0'))
                                          .arg(minutes, 2, 10, QChar('0'))
                                          .arg(seconds, 2, 10, QChar('0'));
        return qstr;
    }
}

//! Установка списка ассоциативного списка московского времени.
//! @param pMoscowTimeVector - [in] Список значений московского времени, (индекс == системные сек.).
void CDisplayTimeOperator::SetMoscowTimeVector(boost::shared_ptr<MoscowTimeVector> pMoscowTimeVector)
{
    m_pMoscowTimeVector = pMoscowTimeVector;
}

//! Установка вида отображаемого времени (московское или системное).
//! @param isMoscowTimeVector - [in] true - отображается московское время, false - системное время.
void CDisplayTimeOperator::SetVisibleTimeMode(bool isMoscowTimeVector)
{
    m_isMoscowTime = isMoscowTimeVector;
}

//! Установка текущего временного отрезка.
//! @param workSpan - [in] временной диапазон.
void CBoundedTimeOperator::SetWorkSpan(TimeRange workSpan)
{
    m_currentTimeRange.timeStart = workSpan.timeStart;
    m_currentTimeRange.timeFinish = workSpan.timeFinish;
    //длительность текущего отрезка
    m_duration = m_currentTimeRange.timeFinish - m_currentTimeRange.timeStart;
    m_center = m_currentTimeRange.timeStart + (m_duration / 2);
}

//! Установка всего временного диапазона (для сессии)
//! @param start - [in] время начала;
//! @param finish - [in] время конца.
void CBoundedTimeOperator::SetAllSpan(TimeCyclic start, TimeCyclic finish)
{
    m_allTimeRange.timeStart = start;
    m_allTimeRange.timeFinish = finish;
    //длительность сессии
    m_allDuration = m_allTimeRange.timeFinish - m_allTimeRange.timeStart;
}

//! Установка центра текущего (отображаемого) отрезка.
//! @param time - [in] время.
void CBoundedTimeOperator::SetCenter(TimeCyclic time)
{
    m_center = time;
}

//! Смещение центра текущего (отображаемого) отрезка.
//! @param shift - [in] смещение.
void CBoundedTimeOperator::ShiftCenter(int64_t shift)
{
    if ((-1 * shift) > (int64_t)m_center)
    {
        m_center = 0;
    }
    else
    {
        m_center += shift;
    }
}

//! Установка длительности.
//! @param duration - [in] длительность.
void CBoundedTimeOperator::VisibleScreen(TimeCyclic duration)
{
    if (duration > m_allDuration)
    {
        duration = m_allDuration;
    }

    // допустимая область слева
    if (m_center < (m_allTimeRange.timeStart + (duration / 2)))
    {
        m_currentTimeRange.timeStart = m_allTimeRange.timeStart;
    }
    else
    {
        m_currentTimeRange.timeStart = m_center - (duration / 2);
    }

    // допустимая область справа
    if (m_center >(m_allTimeRange.timeFinish - (duration / 2)))
    {
        m_currentTimeRange.timeFinish = m_allTimeRange.timeFinish;
        // нужно поменять стартовую точку
        m_currentTimeRange.timeStart = m_currentTimeRange.timeFinish - duration;
    }
    else
    {
        // можно смещаться относ. старта
        m_currentTimeRange.timeFinish = m_currentTimeRange.timeStart + duration; 
    }

    m_duration = duration;
}

//! Возвращает длительность текущего временного отрезка.
//! @return длительность текущего отрезка.
TimeCyclic CBoundedTimeOperator::GetDuration()
{
    return m_duration;
}

//! Возвращает длительность всей записи.
//! @return вся длительность.
TimeCyclic CBoundedTimeOperator::GetAllDuration()
{
    return m_allDuration;
}

//! Возвращает диапазон текущего отрезка.
//! @return диапазон отрезка.
TimeRange CBoundedTimeOperator::GetCurrentTimeRange()
{
    return m_currentTimeRange;
}

//! Виден ли временная метка в текущем временном отрезке.
//! @param time - [in] Запрашиваемое время.
//! @return true - врем. метка находится внутри текущего отрезка, false - иначе.
bool CBoundedTimeOperator::IsVisible(TimeCyclic time)
{
    if (time < m_currentTimeRange.timeStart || time > m_currentTimeRange.timeFinish)
    {
        return false;
    }
    return true;
}

//! Настройка скролбара при изменении масштаба.
//! @param coef - [out] коэффициенты для ScrollBar.
void CBoundedTimeOperator::ComputeTimeRange(int* coef)
{
    //отношение размера ScrollBar и длительности. Если длительность > 10000, нужно считать отношение
    m_coefConversion = 1;
    if (m_allDuration > 10000)
    {
        m_coefConversion = m_allDuration / 10000;
    }
    // max
    coef[0] = (m_allDuration / m_coefConversion) - (m_duration / m_coefConversion);
    // step
    coef[1] = m_duration / m_coefConversion;
    // value
    coef[2] = (m_currentTimeRange.timeStart - m_allTimeRange.timeStart) / m_coefConversion;
    // положение "ползунка"
    m_position = coef[2];
}

//! Установка параметров при заданном положении "ползунка" на экране.
//! @param position - [in] положение ползунка (ScrollBar::value).
void CBoundedTimeOperator::SetHorizontScroll(int position)
{
    if (m_position != position)
    {
        m_currentTimeRange.timeStart = m_allTimeRange.timeStart +
            (TimeCyclic)position * m_coefConversion;
        m_center = m_currentTimeRange.timeStart + (m_duration / 2);
        m_position = position;
    }
}

//! Конструктор.
CGroupOfChart::CGroupOfChart() : m_currentIndex(FRAME_MODE_INDEX), m_count(2)
{
}

//! Возвращает количество каналов для текущего режима (экрана).
//! @return количество каналов.
int CGroupOfChart::GetChannelCount()
{
    return m_codeByIndex[m_currentIndex].size();
}

bool CGroupOfChart::IsEventChart()
{
    if (m_currentIndex == EVENT_MODE_INDEX)
    {
        return true;
    }
    return false;
}

//! Возвращает ассоциативный код канала по его порядковому индексу в отображаемом списке.
//! @param idx - [in] порядковый номер канала.
//! @return Ассоциативный код для канала.
uint32_t CGroupOfChart::GetCodeByIndex(int idx)
{
    return m_codeByIndex[m_currentIndex][idx];
}

//! Возвращает записи для канала по его порядковому индексу.
//! @param idx - [in] порядковый номер канала.
//! @return массив записей для канала.
const ChannelRecords& CGroupOfChart::GetChannelByIndex(int idx)
{
    uint32_t code = m_codeByIndex[m_currentIndex][idx];
    if (m_currentIndex == EVENT_MODE_INDEX)
    {
        return m_eventChart[code];
    }
    return m_frameChart[code];
}

//! Выбор другого экрана (режима) для отображения.
void CGroupOfChart::SelectNext()
{
    m_currentIndex++;
    if (m_currentIndex >= m_count)
    {
        m_currentIndex = 0;
    }
}

//! Инициализация связи информации о каналах событий и массива записей в каналы.
//! @param screenData - [in] список данных о каналах.
void CGroupOfChart::InitChartEvets(std::vector<std::pair<DataId, std::string>> events)
{
    m_eventChart.clear();
    m_codeByIndex[EVENT_MODE_INDEX].clear();
    for (size_t i = 0; i < events.size(); i++)
    {
        m_eventChart[events[i].first.uint32].clear();
        m_codeByIndex[EVENT_MODE_INDEX].push_back(events[i].first.uint32);
    }
}

//! Инициализация связи информации о каналах кадров и массива записей в каналы.
//! @param screenData - [in] список данных о каналах.
void CGroupOfChart::InitChartFrames(std::vector<std::pair<DataId, std::string>> events)
{
    m_frameChart.clear();
    m_codeByIndex[FRAME_MODE_INDEX].clear();
    for (size_t i = 0; i < events.size(); i++)
    {
        m_frameChart[events[i].first.uint32].clear();
        m_codeByIndex[FRAME_MODE_INDEX].push_back(events[i].first.uint32);
    }
}

//! Добавление/изменение записей в ассоциативный массив.
//! @param events - [in] список событий.
void CGroupOfChart::SetEventSignal(const std::vector<CEvent>& events)
{
    for (size_t i = 0; i < events.size(); i++)
    {
        DataId eventDataId = events[i].GetDataId();

        if (eventDataId.type == DataId::DT_EVENT_LONG)
        { 
            if (eventDataId.index == 0)
            {
                m_eventChart[eventDataId.uint32].push_back(events[i]);
            }
            else
            {
                eventDataId.index = 0;
                m_eventChart[eventDataId.uint32].push_back(events[i]);
            }
        }
        else if (eventDataId.type == DataId::DT_REF_RASTER_FRAME)
        {
            eventDataId.type = DataId::DT_RASTER_FRAME;
            m_eventChart[eventDataId.uint32].push_back(events[i]);
        }
        else if (eventDataId.type == DataId::DT_REF_VECTOR_FRAME)
        {
            eventDataId.type = DataId::DT_VECTOR_FRAME;
            m_eventChart[eventDataId.uint32].push_back(events[i]);
        }
        else if (eventDataId.type == DataId::DT_REF_AUDIO)
        {
            eventDataId.type = DataId::DT_AUDIO_BUFFER;
            m_eventChart[eventDataId.uint32].push_back(events[i]);
        }
        else
        {
            m_eventChart[eventDataId.uint32].push_back(events[i]);
        }
    }
}

//! Установка списка событий в режиме кадров.
//! @param events - [in] список событий.
void CGroupOfChart::SetFrameSignal(const std::vector<CEvent>& events)
{
    for (size_t i = 0; i < events.size(); i++)
    {
        DataId eventDataId = events[i].GetDataId();

        if (eventDataId.type == DataId::DT_EVENT_LONG)
        {
            if (eventDataId.index == 0)
            {
                m_frameChart[eventDataId.uint32].push_back(events[i]);
            }
            else
            {
                eventDataId.index = 0;
                m_frameChart[eventDataId.uint32].push_back(events[i]);
            }
        }
        else if (eventDataId.type == DataId::DT_REF_RASTER_FRAME)
        {
            eventDataId.type = DataId::DT_RASTER_FRAME;
            m_frameChart[eventDataId.uint32].push_back(events[i]);
        }
        else if (eventDataId.type == DataId::DT_REF_VECTOR_FRAME)
        {
            eventDataId.type = DataId::DT_VECTOR_FRAME;
            m_frameChart[eventDataId.uint32].push_back(events[i]);
        }
        else if (eventDataId.type == DataId::DT_REF_AUDIO)
        {
            eventDataId.type = DataId::DT_AUDIO_BUFFER;
            m_frameChart[eventDataId.uint32].push_back(events[i]);
        }
        else
        {
            m_frameChart[eventDataId.uint32].push_back(events[i]);
        }
    }
}

}