#include <QGridLayout>
#include <QPainter>
#include <QLabel>
#include <QMouseEvent>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "chartwidget.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

void MainWindow::MySetupUi()
{
    QPixmap * m_pixmap = new QPixmap(300, 200);

    m_pixmap->fill(QColor("white"));

    QPainter painter(m_pixmap);
    QPen pen(Qt::red, 2);
    painter.setPen(pen);
    painter.drawLine(1, 1, 250, 190);

    QLabel *labeltime = new QLabel;
    labeltime->setPixmap(*m_pixmap);

    QSize chartSize(2, 2);
    chart::CChartWidget* chart = new chart::CChartWidget();
    
    setCentralWidget(chart);
}

void MainWindow::mousePressEvent(QMouseEvent *mouseEvent)
{
    update();
    ui->centralWidget->update();
}

void MainWindow::paintEvent(QPaintEvent *paintEvent)
{
QPainter painter(this);
QPen redPen(Qt::red,10);
redPen.setCapStyle(Qt::RoundCap);
painter.setPen(redPen);
painter.drawPoint(this->x,this->y);
}

MainWindow::~MainWindow()
{
    delete ui;
}
