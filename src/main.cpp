#include "FindEventsWidget.h"
#include "FindEventsManager.h"
#include "chartwidget.h"
#include "includes/Event.h"
#include "includes/DataId.h"
#include "includes/Time.h"
#include "includes/Format.h"
#include <QApplication>

const size_t FRAME_COUNT = 14;
const size_t EVENT_COUNT = 11;
const chart::TimeCyclic CHART_DURATION = chart::ONE_SECOND * 3600;
const uint32_t MAX_EVENTS_IN_CHANNEL   = 100;
const uint32_t COUNT_EVENT_TYPES       = 9;

//! Генерация всех событий одного канала.
//! @param channelIndex - [in] индекс канала;
//! @param isFrameEment - [in] true - кадры, false - события.
//! @return массив событий одного канала.
std::vector<chart::CEvent> GenerateChannelEvents(uint16_t channelIndex, bool isFrameEvent)
{
    uint32_t koefCurvePulse = RAND_MAX / CHART_DURATION;
    uint32_t koefCurveWindowsCount = RAND_MAX / MAX_EVENTS_IN_CHANNEL;
    uint32_t koefType = RAND_MAX / COUNT_EVENT_TYPES;
    
    uint32_t indexOne = 0;
    uint32_t indexTwo = 0;

    std::vector<chart::CEvent> events;
    std::vector<uint32_t> randCode;
    chart::TimeCyclic timePulse = 0;
    int curveCount = (std::rand() / koefCurveWindowsCount) + 1;
    chart::TimeCyclic deltaTime = CHART_DURATION / curveCount * 2;

        
    uint8_t idxType = chart::DataId::DT_EVENT_LONG;
    if (!isFrameEvent)
    {
        idxType = std::rand() / koefType;
        while (idxType == chart::DataId::DT_EVENT_LONG)
        {
            idxType = std::rand() / koefType;
        }

        channelIndex += (FRAME_COUNT - 1);
    }

    chart::DataId dataIdUnit(idxType, channelIndex, 0);

    for (int j = 0; j < curveCount; j++)
    {
        chart::TimeCyclic randDeltaTime = (chart::TimeCyclic)std::rand() * deltaTime / (chart::TimeCyclic)RAND_MAX;
        timePulse += randDeltaTime;

        if (timePulse > CHART_DURATION)
        {
            break;
        }

        if (idxType == chart::DataId::DT_EVENT_LONG && (j > 0 && dataIdUnit.index == 0))
        {
            dataIdUnit.index = 1;
        }
        else
        {
            dataIdUnit.index = 0;
        }

        events.push_back(chart::CEvent(dataIdUnit, nullptr, 0, timePulse, timePulse));
    }

    return events;
}

//! Генерация массива событий или кадров.
//! @param isFrameEmvents - [in] true - формируются кадры, false - формируются события.
//! @return массив всех кадров или событий окна.
std::vector<chart::CEvent> GenerateEvents(bool isFrameEvents)
{
    std::vector<chart::CEvent> events;

    size_t cntEvents = FRAME_COUNT;
    if (!isFrameEvents)
    {
        cntEvents = EVENT_COUNT;
    }

    for (size_t numEvent = 0; numEvent < cntEvents; ++numEvent)
    {
        std::vector<chart::CEvent> oneChannel = GenerateChannelEvents(numEvent, isFrameEvents);
        events.insert(events.end(), oneChannel.begin(), oneChannel.end());
    }

    return events;
}

//! Установка имен каналов менеджера поиска событий.
//! @param manager     - [in] менеджер окна поиска событий;
//! @param chartFrames - [in] массив кадров;
//! @param chartEvents - [in] массив событий.
void SetChannelNames(chart::CFindEventsManager &manager, 
                         std::vector<chart::CEvent> chartFrames, 
                         std::vector<chart::CEvent> chartEvents)
{
    std::vector<std::pair<chart::DataId, std::string>> eventNames;
    std::vector<std::pair<chart::DataId, std::string>> frameNames;

    std::vector<uint32_t> frameCodes;
    for (size_t numFrame = 0; numFrame < chartFrames.size(); numFrame++)
    {
        chart::DataId frDataId = chartFrames[numFrame].GetDataId();
        frDataId.index = 0;
        if (std::find(frameCodes.begin(), frameCodes.end(), frDataId.uint32) == frameCodes.end())
        {
            frameCodes.push_back(frDataId.uint32);
        }
    }

    std::vector<uint32_t> eventCodes;
    for (size_t numEvent = 0; numEvent < chartEvents.size(); numEvent++)
    {
        if (std::find(eventCodes.begin(), eventCodes.end(), 
            chartEvents[numEvent].GetDataId().uint32) == eventCodes.end())
        {
            eventCodes.push_back(chartEvents[numEvent].GetDataId().uint32);
        }
    }

    for (size_t numFrameChannel = 0; numFrameChannel < frameCodes.size(); numFrameChannel++)
    {
        chart::DataId frDataId;
        frDataId.uint32 = frameCodes[numFrameChannel];
        std::string nameChannel = FormatString("channel #%d", frDataId.id);
        frameNames.push_back(std::make_pair(frDataId, nameChannel));
    }

    for (size_t numEventChannel = 0; numEventChannel < eventCodes.size(); ++numEventChannel)
    {
        chart::DataId evDataId;
        evDataId.uint32 = eventCodes[numEventChannel];
        std::string nameChannel = FormatString("channel #%d", evDataId.id);
        eventNames.push_back(std::make_pair(evDataId, nameChannel));
    }

    manager.SetConfiguredDataId(eventNames, frameNames);
}

//! Генерация массива московского времени, соответствующего секундам системного.
//! @return массив московского времени.
boost::shared_ptr<chart::MoscowTimeVector> GenerateMoscowTime()
{
    boost::shared_ptr<chart::MoscowTimeVector> pMoscowTimeVector(new chart::MoscowTimeVector());

    for (uint64_t i = 0; i < CHART_DURATION; i += chart::ONE_SECOND)
    {
        pMoscowTimeVector->push_back((i * chart::ONE_SECOND) + (chart::ONE_SECOND * 18000));
    }

    return pMoscowTimeVector;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    std::vector<chart::CEvent> chartFrames = GenerateEvents(true);
    std::vector<chart::CEvent> chartEvents = GenerateEvents(false);

    chart::CFindEventsWidget* chart = new chart::CFindEventsWidget();

    chart::CFindEventsManager eventManager(chart, 0);

    SetChannelNames(eventManager, chartFrames, chartEvents);
    eventManager.InitRecords(chartEvents, chartFrames);
    

    chart::TimeRange allTime(0, CHART_DURATION);
    
    eventManager.InitRecordsParameters(allTime, GenerateMoscowTime());

    chart->show();

    return a.exec();
}
