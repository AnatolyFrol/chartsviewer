//! @file FindEventsWidget.cpp
//! Реализация класса chart::CFindEventsWidget.

#include <QLabel>
#include <QGraphicsScene>
#include <QHBoxLayout>
#include <QtGui/QScrollBar>
#include <QMouseEvent>

#include "FindEventsWidget.h"


namespace chart
{

//! Конструктор.
//! @param parent - [in] родительское окно.
CFindEventsWidget::CFindEventsWidget(QWidget *parent/* = 0*/) : QWidget(parent)
{
    QHBoxLayout *allLayout = new QHBoxLayout(this);
    allLayout->setMargin(0);
    allLayout->setSpacing(0);

    QVBoxLayout *chartLayout = new QVBoxLayout();
    chartLayout->setMargin(0);
    chartLayout->setSpacing(0);
    m_pChart = new CChartWidget(this);
    m_pChart->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_pChart->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    m_pHorizScrollBar = new QScrollBar(Qt::Horizontal, this);
    m_buttonMode = new QPushButton("Mode");
    //ширина кнопки = 100
    m_buttonMode->setMaximumWidth(100);
    chartLayout->addWidget(m_pChart);
    chartLayout->addWidget(m_pHorizScrollBar);
    chartLayout->addWidget(m_buttonMode, 0, Qt::AlignRight);

    //ширина левой области = 220
    m_pNames = new QScrollArea(this);
    m_pNames->setMaximumWidth(220);
    m_pNames->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_pNames->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    //ширина левой области = 220
    m_pTimeScreen = new QLabel();
    m_pTimeScreen->setMaximumWidth(220);
    m_pTimeScreen->setMinimumHeight(CLinesPainter::bandHeight + 5);
    m_pTimeScreen->setMaximumHeight(CLinesPainter::bandHeight + 5);

    m_pVirtBoxLayout = 0;
    QVBoxLayout *leftLayout = new QVBoxLayout();
    leftLayout->setMargin(0);
    leftLayout->setSpacing(0);
    leftLayout->addWidget(m_pNames);
    leftLayout->addWidget(m_pTimeScreen);

    allLayout->addLayout(leftLayout);
    allLayout->addLayout(chartLayout);

    setMouseTracking(true);

    connect(m_pChart->verticalScrollBar(), SIGNAL(valueChanged(int)),
        this, SLOT(OnTextVerticalPos(int)));
        
    connect(m_pHorizScrollBar, SIGNAL(valueChanged(int)),
        this, SLOT(OnChartHorizontalPos(int)));

    connect(m_pChart, SIGNAL(TimeMarkChanged(QPoint)),
        this, SLOT(OnTimeMarkChanged(QPoint)));

    connect(m_pChart, SIGNAL(EvtTempTimePosition(int)),
        this, SLOT(OnTempTimePosition(int)));

    connect(m_buttonMode, SIGNAL(clicked(bool)),
        this, SLOT(OnChangeMode()));
}

//! Деструктор.
CFindEventsWidget::~CFindEventsWidget()
{
}

//! Выполянят перевод GUI.
void CFindEventsWidget::RetranslateUi()
{
    m_buttonMode->setText("Mode");
}

//! Определение праметров горизонтального ScrollBar.
//! @param max   - [in] Максимальное значение;
//! @param step  - [in] Размер ползунка;
//! @param value - [in] Значение в котором находится ползунок.
void CFindEventsWidget::SetHorizontalScroll(int max, int step, int value)
{
    m_pHorizScrollBar->setMinimum(0);
    m_pHorizScrollBar->setMaximum(max);
    m_pHorizScrollBar->setPageStep(step);
    m_pHorizScrollBar->setValue(value);
}

//! Установка высоты окна.
//! @param height - [in] Высота.
void CFindEventsWidget::SetHeight(int height)
{
    m_pChart->SetHeight(height);
}

//! Установка картинки в виджет.
//! @param pixmap - [in] Картинка.
void CFindEventsWidget::UpdatePixmap(const QPixmap& pixmap)
{
    m_pChart->SetPixmap(pixmap);
}

//! Создание надписей - имен каналов.
//! @param nameIdList - [in] Список имен каналов.
void CFindEventsWidget::DrawText(std::vector<std::pair<DataId, std::string>> nameIdList)
{
    int height = CLinesPainter::bandHeight * (nameIdList.size() + 2);
    QPixmap pixmapLine = QPixmap(217, 1);

    QFont font = QFont("Arial", 12, QFont::Black);

    QPainter painter(&pixmapLine);
    painter.drawLine(0, 0, 217, 0);

    QWidget *labelNames = new QWidget;

    labelNames->setFont(font);
    QPalette sample_palette;
    sample_palette.setColor(QPalette::Window, Qt::white);
        
    labelNames->setPalette(sample_palette);

    if (m_pVirtBoxLayout != 0)
    {
        delete m_pVirtBoxLayout;
    }

    m_pVirtBoxLayout = new QVBoxLayout(labelNames);
    m_pVirtBoxLayout->setMargin(0);
    m_pVirtBoxLayout->setSpacing(0);
    m_pVirtBoxLayout->setAlignment(Qt::AlignRight);

    m_selectedIndex = -1;
    for (int i = 0; i < nameIdList.size(); i++)
    {
        QLabel *textLabel = new QLabel(QString(nameIdList[i].second.c_str()));

        QWidget *nameChannel = new QWidget;
        QHBoxLayout *hBoxLayout = new QHBoxLayout(nameChannel);
        hBoxLayout->setMargin(0);
        hBoxLayout->setSpacing(0);

        QPushButton *buttonLeft = new QPushButton;
        buttonLeft->setMaximumWidth(32);
        buttonLeft->setMaximumHeight(32);
        buttonLeft->setIcon(QIcon(":/icons/ico_dec1.ico"));
        buttonLeft->setIconSize(QSize(32, 32));
        buttonLeft->setStyleSheet("background-color: #DCDCDC");
        buttonLeft->hide();

        QPushButton *buttonRight = new QPushButton;
        buttonRight->setMaximumWidth(32);
        buttonRight->setMaximumHeight(32);
        buttonRight->setIcon(QIcon(":/icons/ico_inc1.ico"));
        buttonRight->setIconSize(QSize(32, 32));
        buttonRight->setStyleSheet("background-color: #DCDCDC");
        buttonRight->hide();

        connect(buttonLeft, SIGNAL(clicked(bool)),
            this, SLOT(OnShiftMarkLeft()));
        connect(buttonRight, SIGNAL(clicked(bool)),
            this, SLOT(OnShiftMarkRight()));

        hBoxLayout->addWidget(textLabel, 0, Qt::AlignLeft);
        hBoxLayout->addStretch(10);
        hBoxLayout->addWidget(buttonLeft, 0, Qt::AlignRight);
        hBoxLayout->addWidget(buttonRight, 0, Qt::AlignRight);
        nameChannel->setFixedHeight(CLinesPainter::bandHeight - 1);
        nameChannel->setMaximumWidth(217);

        m_pVirtBoxLayout->addWidget(nameChannel);

        QLabel *labelBorder = new QLabel;
        labelBorder->setPixmap(pixmapLine);
        labelBorder->setFixedHeight(1);
        m_pVirtBoxLayout->addWidget(labelBorder);
    }

    QWidget *emptyChannel = new QWidget;
    emptyChannel->setFixedHeight(CLinesPainter::bandHeight);
    m_pVirtBoxLayout->addWidget(emptyChannel);

    QWidget *emptyChannel2 = new QWidget;
    emptyChannel2->setFixedHeight(CLinesPainter::bandHeight);
    m_pVirtBoxLayout->addWidget(emptyChannel2);

    QWidget *oldLabelNames = m_pNames->takeWidget();
    if (oldLabelNames != 0)
    {
        delete oldLabelNames;
    }

    m_pNames->setWidget(labelNames);
    m_pNames->verticalScrollBar()->hide();
}

//! Установка указателя на оператор, производящий расчет времени.
//! @param timeOperator - [in] Оператор времени.
void CFindEventsWidget::SetTimeOperator(CDisplayTimeOperator *pTimeOperator)
{
    m_pTimeOperator = pTimeOperator;
    //"00:00:00";
    m_markTimeStr = m_pTimeOperator->GetTimeTextFromScreen(0);
    m_cursorTimeStr = m_pTimeOperator->GetTimeTextFromScreen(0);
}

//! Обновление метки при смещении, изменении размеров.
//! @param markProperty - [in] Параметры выделения канала и времени.
void CFindEventsWidget::UpdateScreen(ChannelMarkProperty markProperty)
{
    if (markProperty.xPosition >= 0)
    {
        m_pChart->SetTimeMark(markProperty.xPosition);
        // обновление надписи
        m_markTimeStr = m_pTimeOperator->CreateTextFromTime(markProperty.timeMark);
        UpdateTimeLabel();
    }
    else
    {
        m_pChart->HideTimeMark();
    }
}

//! Установка метки времени + выделение одного канала.
//! @param markProperty - [in] Параметры выделения канала и времени.
void CFindEventsWidget::SetTimeMark(ChannelMarkProperty markProperty)
{
    if (markProperty.xPosition > 0)
    {
        m_pChart->SetTimeMark(markProperty.xPosition);
    }

    if ((m_selectedIndex >= 0) && (m_selectedIndex != markProperty.channelIndex))
    {
        QWidget *nameChannel = m_pVirtBoxLayout->itemAt(m_selectedIndex * 2)->widget();

        QList<QWidget *> listChildren = nameChannel->findChildren<QWidget*>();

        QWidget *buttonLeft = listChildren[1];
        buttonLeft->setVisible(false);
        QWidget *buttonRight = listChildren[2];
        buttonRight->setVisible(false);

        nameChannel->setStyleSheet("background-color: white; font-family: \"Arial\"; "
            "font-size: 16px; font-weight: bold");
    }
    m_selectedIndex = markProperty.channelIndex;
    if (m_selectedIndex < 0)
    {
        return;
    }

    //индекс в 2 раза больше из-за черты
    QWidget *nameChannel = m_pVirtBoxLayout->itemAt(m_selectedIndex * 2)->widget();
    QList<QWidget *> listChildren = nameChannel->findChildren<QWidget*>();

    QWidget *buttonLeft = listChildren[1];
    buttonLeft->setVisible(true);
    buttonLeft->setEnabled(markProperty.leftShift);
    QString backgroundColor = markProperty.leftShift ? "background-color: #DCDCDC" 
                                                     : "background-color: #F0F0F0";
    buttonLeft->setStyleSheet(backgroundColor);
    QWidget *buttonRight = listChildren[2];
    buttonRight->setVisible(true);
    buttonRight->setEnabled(markProperty.rightShift);
    backgroundColor = markProperty.rightShift ? "background-color: #DCDCDC" 
                                              : "background-color: #F0F0F0";
    buttonRight->setStyleSheet(backgroundColor);

    nameChannel->setStyleSheet("background-color: aqua; font-family: \"Arial\"; "
        "font-size: 16px; font-weight: bold");
}

//! Определение точек-привязок.
//! @param points - [in] точки-привязки.
void CFindEventsWidget::BindingPoints(std::vector<int> *points)
{
    m_pChart->BindingPoints(points);
}

//! Обновление надписи времени.
void CFindEventsWidget::UpdateTimeLabel()
{
    QString timeStr("<font size=8 color=\"#FF0000\">%1</font>   "
        "<font size=6 color=\"#000000\">%2</font>");

    m_pTimeScreen->setText(timeStr.arg(m_markTimeStr, m_cursorTimeStr));
}

//! Обработчик события : нажатие кн. мыши по виджету.
//! @param pEvent - [in] Событие нажатия мыши.
void CFindEventsWidget::mousePressEvent(QMouseEvent *pEvent)
{
    int yCoord = pEvent->y();
    if (m_pChart->verticalScrollBar()->isVisible())
    {
        yCoord += m_pChart->verticalScrollBar()->value();
    }

    int newSelectedIndex = yCoord / CLinesPainter::bandHeight;

    emit EvtChannelSelection(newSelectedIndex);
}

//! Обработчик события : установка вертикального положения окна.
//! @param position - [in] Значение положения скрола.
void CFindEventsWidget::OnTextVerticalPos(int position)
{
    m_pNames->verticalScrollBar()->setValue(position);
}

//! Обработчик события : установка горизонтального скрола.
//! @param position - [in] Значение положения скрола.
void CFindEventsWidget::OnChartHorizontalPos(int position)
{
    emit ChartHorizontalScrollBar(position);
}
    
//! Обработчик события : изменение метки из View.
//! @param position - [in] координата, включающая индекс канала(y) и метку на врем. шкале(x).
void CFindEventsWidget::OnTimeMarkChanged(QPoint position)
{
    m_markTimeStr = m_pTimeOperator->GetTimeTextFromScreen(position.x());
    UpdateTimeLabel();

    int yCoord = position.y();
    if (m_pChart->verticalScrollBar()->isVisible())
    {
        yCoord += m_pChart->verticalScrollBar()->value();
    }

    int newSelectedIndex = yCoord / CLinesPainter::bandHeight;

    position.setY(newSelectedIndex);

    emit EvtTimeMarkChanged(position);
}

//! Обработчик события : обновление строки метки под курсором.
//! @param position - [in] позиция метки на экране в пикселях.
void CFindEventsWidget::OnTempTimePosition(int position)
{
    m_cursorTimeStr = m_pTimeOperator->GetTimeTextFromScreen(position);
    UpdateTimeLabel();
}

//! Обработчик события : смена режима.
void CFindEventsWidget::OnChangeMode()
{
    emit EvtChangeMode();
}

//! Обработчик события : смещение метки влево.
void CFindEventsWidget::OnShiftMarkLeft()
{
    emit EvtTimeMarkShift(-1);
}
//! Обработчик события : смещение метки вправо.
void CFindEventsWidget::OnShiftMarkRight()
{
    emit EvtTimeMarkShift(1);
}

//! Обработчик события : нажатие кнопки мыши по виджету.
//! @param pEvent - [in] объект-событие.
void CFindEventsWidget::mouseMoveEvent(QMouseEvent *pEvent)
{
    QWidget::mouseMoveEvent(pEvent);
}

//! Обработчик события : нажатие кнопки на клавиатуре.
//! @param pEvent - [in] объект-событие.
void CFindEventsWidget::keyPressEvent(QKeyEvent *pEvent)
{
    if (pEvent->text() == "+")
    {
        emit ZoomIn();
    }
    if (pEvent->text() == "-")
    {
        emit ZoomOut();
    }
}

//! Обработчик события : изменение размера виджета.
//! @param pEvent - [in] объект-событие.
void CFindEventsWidget::resizeEvent(QResizeEvent* pEvent)
{
    emit SizeChanged(m_pChart->size());
}

} // namespace chart
